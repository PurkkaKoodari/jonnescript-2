#JonneScript 2

JonneScript 2 is a scripting language based on the Finnish language. This repository contains the specification (todo) and the reference interpreter, written in Java 8.

##Features

  - Basic types: number, string, boolean, array, function, null.
  - JavaScript-like object system: everything is an object.
  - Operators: arithmetic, logic, comparison.
  - Standard library (todo), mostly written in JonneScript 2 itself. Includes functions for I/O and things such as regex.

##Examples

###Hello World

    tulosta aja teksti Hello, World! lopeta valmis