2.0-a7
 - added number and array repr
 - added spread operator
 - added integer optimization to numbers
 - performance improved majorly

2.0-a6
 - added multi-word tokens
 - added while
 - added try/catch
 - added raise
 - added varargs
 - added parenthesis
 - added identity operator
 - moved all operators to native operator methods
 - renamed comparison operators
 - assignment is now left-associative again

2.0-a5
 - added stack trace to runtime exceptions
 - added operator methods
 - added else
 - assignment is now right-associative

2.0-a4
 - added object system
 - added object literals
 - added this
 - added closures
 - added repr, length, property operators
 - removed length function

2.0-a3
 - added command line options
 - added array literals
 - added until loop
 - added modulo, string split, or, and, equal
 - normalize line separators
 - bug fixes

2.0-a2
 - switch to built-in Unicode strings
 - added REPL environment
 - added numeric division, not, xor, greater-than, less-than
 - added eval by calling string
 - added exit function
 - added version variable
 - bug fixes

2.0-a1
 - original release