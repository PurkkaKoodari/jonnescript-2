package net.pietu1998.jonnescript2.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigInteger;

import net.pietu1998.jonnescript2.lexer.NumberParser;
import net.pietu1998.jonnescript2.lexer.StringToken;
import net.pietu1998.unicode.UnicodeString;

import org.junit.Test;

public class NumberParserTest {

	private static void assertNumberParse(String text, BigInteger value) {
		StringToken token = new StringToken(new UnicodeString(), new UnicodeString(text), "", 0);
		BigInteger number = NumberParser.tryParseNumber(token);
		assertNotNull("onnistuu: \"" + text + "\"", number);
		assertEquals("laske: \"" + text + "\" = " + value, number, value);
	}

	private static void assertNumberParseFail(String text) {
		StringToken token = new StringToken(new UnicodeString(), new UnicodeString(text), "", 0);
		BigInteger number = NumberParser.tryParseNumber(token);
		assertNull("epäonnistuu: \"" + text + "\"", number);
	}

	@Test
	public void testTryParseNumberValid() {
		// test 0
		assertNumberParse("nolla", BigInteger.valueOf(0));
		// test 1-9
		assertNumberParse("yksi", BigInteger.valueOf(1));
		assertNumberParse("kahdeksan", BigInteger.valueOf(8));
		// test 10, 100
		assertNumberParse("kymmenen", BigInteger.valueOf(10));
		assertNumberParse("sata", BigInteger.valueOf(100));
		// test 11-19
		assertNumberParse("yksitoista", BigInteger.valueOf(11));
		assertNumberParse("viisitoista", BigInteger.valueOf(15));
		// test 20-90, tens
		assertNumberParse("kaksikymmentä", BigInteger.valueOf(20));
		assertNumberParse("neljäkymmentä", BigInteger.valueOf(40));
		// test 20-99
		assertNumberParse("kolmekymmentäyhdeksän", BigInteger.valueOf(39));
		assertNumberParse("kahdeksankymmentäneljä", BigInteger.valueOf(84));
		// test 100-109
		assertNumberParse("satayksi", BigInteger.valueOf(101));
		assertNumberParse("satakuusi", BigInteger.valueOf(106));
		// test 110
		assertNumberParse("satakymmenen", BigInteger.valueOf(110));
		// test 120-190, tens
		assertNumberParse("satakaksikymmentä", BigInteger.valueOf(120));
		assertNumberParse("satayhdeksänkymmentä", BigInteger.valueOf(190));
		// test 111-119
		assertNumberParse("sataneljätoista", BigInteger.valueOf(114));
		// test 120-199
		assertNumberParse("sataviisikymmentäyhdeksän", BigInteger.valueOf(159));
		assertNumberParse("sataseitsemänkymmentäkolme", BigInteger.valueOf(173));
		// test 200-900, hundreds
		assertNumberParse("kuusisataa", BigInteger.valueOf(600));
		assertNumberParse("kahdeksansataa", BigInteger.valueOf(800));
		// test 200-990, tens
		assertNumberParse("neljäsataakaksikymmentä", BigInteger.valueOf(420));
		assertNumberParse("viisisataakymmenen", BigInteger.valueOf(510));
		assertNumberParse("yhdeksänsataakahdeksankymmentä", BigInteger.valueOf(980));
		// test 200-999
		assertNumberParse("kaksisataaneljäkymmentäseitsemän", BigInteger.valueOf(247));
		assertNumberParse("kuusisataayhdeksän", BigInteger.valueOf(609));
		assertNumberParse("kuusisataakuusikymmentäkuusi", BigInteger.valueOf(666));
		assertNumberParse("seitsemänsataakahdeksankymmentäyhdeksän", BigInteger.valueOf(789));
		// test 1000
		assertNumberParse("tuhat", BigInteger.valueOf(1000));
		// test possible edge cases
		assertNumberParse("tuhatsata", BigInteger.valueOf(1100));
		assertNumberParse("tuhatkymmenen", BigInteger.valueOf(1010));
		assertNumberParse("tuhatsatakymmenen", BigInteger.valueOf(1110));
		// test 1000-1999
		assertNumberParse("tuhatyksi", BigInteger.valueOf(1001));
		assertNumberParse("tuhatsatayksitoista", BigInteger.valueOf(1111));
		assertNumberParse("tuhatneljäsataakahdeksan", BigInteger.valueOf(1408));
		assertNumberParse("tuhatyhdeksänsataaneljäkymmentäkahdeksan", BigInteger.valueOf(1948));
		// test 2000-9000, thousands
		assertNumberParse("kolmetuhatta", BigInteger.valueOf(3000));
		assertNumberParse("yhdeksäntuhatta", BigInteger.valueOf(9000));
		// test 2000-9999
		assertNumberParse("kaksituhattakuusitoista", BigInteger.valueOf(2016));
		assertNumberParse("kolmetuhattakaksisataaseitsemäntoista", BigInteger.valueOf(3217));
		assertNumberParse("viisituhattakolmesataakahdeksankymmentäkaksi", BigInteger.valueOf(5382));
		assertNumberParse("seitsemäntuhattakaksisataakolmekymmentäneljä", BigInteger.valueOf(7234));
		// test 10000-999000, thousands
		assertNumberParse("kymmenentuhatta", BigInteger.valueOf(10000));
		assertNumberParse("viisitoistatuhatta", BigInteger.valueOf(15000));
		assertNumberParse("kuusikymmentäkaksituhatta", BigInteger.valueOf(62000));
		assertNumberParse("kahdeksankymmentäkolmetuhatta", BigInteger.valueOf(83000));
		assertNumberParse("sataneljätuhatta", BigInteger.valueOf(104000));
		assertNumberParse("kaksisataakaksikymmentäseitsemäntuhatta", BigInteger.valueOf(227000));
		assertNumberParse("viisisataaneljäkymmentäkuusituhatta", BigInteger.valueOf(546000));
		assertNumberParse("kahdeksansataayhdeksäntoistatuhatta", BigInteger.valueOf(819000));
		// test 10000-999999
		assertNumberParse("neljäkymmentätuhattayhdeksänsataakahdeksankymmentäkahdeksan", BigInteger.valueOf(40988));
		assertNumberParse("viisisataaneljätuhattakymmenen", BigInteger.valueOf(504010));
		assertNumberParse("kahdeksansataaseitsemänkymmentätuhattaviisisataakuusikymmentä", BigInteger.valueOf(870560));
		assertNumberParse("yhdeksänsataaneljäkymmentäkolmetuhattakolmesataaviisitoista", BigInteger.valueOf(943315));
		// test powers of 1e6
		assertNumberParse("miljoona", BigInteger.valueOf(1000000));
		assertNumberParse("biljoona", BigInteger.valueOf(1000000000000L));
		assertNumberParse("vigintiljoona", new BigInteger("1000000000000".replace("0", "0000000000")));
		// test 2-9 times powers of 1e6
		assertNumberParse("kaksimiljoonaa", BigInteger.valueOf(2000000));
		assertNumberParse("kuusitriljoonaa", BigInteger.valueOf(6000000000000000000L));
		assertNumberParse("yhdeksänvigintiljoonaa", new BigInteger("9000000000000".replace("0", "0000000000")));
		// test combined powers of 1e6
		assertNumberParse("biljoonamiljoona", BigInteger.valueOf(1000001000000L));
		assertNumberParse("kahdeksantriljoonaaviisibiljoonaa", BigInteger.valueOf(8000005000000000000L));
		assertNumberParse("kuusitriljoonaayhdeksänbiljoonaakaksimiljoonaaneljä",
				BigInteger.valueOf(6000009000002000004L));
		// test 10-999 times powers of 1e6
		assertNumberParse("viisitoistamiljoonaa", BigInteger.valueOf(15000000));
		assertNumberParse("yhdeksänsataayhdeksänkymmentäyhdeksäntuhattayhdeksänsataayhdeksänkymmentäyhdeksänbiljoonaa",
				BigInteger.valueOf(999999000000000000L));
		// test those combined
		assertNumberParse("kolmekymmentäkaksibiljoonaaseitsemänkymmentätuhattakolmesataa",
				BigInteger.valueOf(32000000070300L));
		assertNumberParse("kolmekymmentäkaksibiljoonaaseitsemänkymmentätuhattakolmesataamiljoonaa",
				BigInteger.valueOf(32070300000000L));
		assertNumberParse("yhdeksänsataayhdeksänkymmentäyhdeksäntuhattayhdeksänsataayhdeksänkymmentäyhdeksänbiljoonaa"
				+ "yhdeksänsataayhdeksänkymmentäyhdeksäntuhattayhdeksänsataayhdeksänkymmentäyhdeksänmiljoonaa"
				+ "yhdeksänsataayhdeksänkymmentäyhdeksäntuhattayhdeksänsataayhdeksänkymmentäyhdeksän",
				BigInteger.valueOf(999999999999999999L));
	}

	@Test
	public void testTryParseNumberInvalid() {
		// test empty string
		assertNumberParseFail("");
		// test random crap
		assertNumberParseFail("asjfdiudshfosd");
		assertNumberParseFail(")#=@'*;|");
		// test valid parts alone
		assertNumberParseFail("sataa");
		assertNumberParseFail("kymmentä");
		assertNumberParseFail("toista");
		assertNumberParseFail("tuhatta");
		assertNumberParseFail("miljoonaa");
		// test concatenated digits
		assertNumberParseFail("kaksineljä");
		assertNumberParseFail("yksikahdeksan");
		// test 0 in weird places
		assertNumberParseFail("nollatoista");
		assertNumberParseFail("nollakymmentä");
		assertNumberParseFail("kaksikymmentänolla");
		assertNumberParseFail("nollasataaviisi");
		assertNumberParseFail("neljäsataanolla");
		assertNumberParseFail("nollamiljoonaa");
		// test 1 in weird places
		assertNumberParseFail("yksikymmentä");
		assertNumberParseFail("yksisataa");
		assertNumberParseFail("yksituhatta");
		assertNumberParseFail("yksibiljoonaa");
		// test 1-9 in weird places
		assertNumberParseFail("yksikymmenen");
		assertNumberParseFail("kaksisata");
		assertNumberParseFail("kolmetuhat");
		assertNumberParseFail("neljätriljoona");
		assertNumberParseFail("yksitrigintiljoona");
		// test powers of 1e6 in wrong order
		assertNumberParseFail("miljoonabiljoona");
		assertNumberParseFail("tuhatmiljoonaatrigintiljoona");
	}

}
