package net.pietu1998.jonnescript2.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.pietu1998.unicode.UnicodeString;

import org.junit.Test;

public class UnicodeStringTest {

	private static final String EMPTY_STRING = "";
	private static final String TEST_STRING = "TEST string\n";
	private static final String SUPPL_STRING = "\uD83D\uDE0A";

	@Test
	public void testCreateEmpty() {
		new UnicodeString();
	}

	@Test
	public void testCreateContent() {
		new UnicodeString(EMPTY_STRING);
		new UnicodeString(TEST_STRING);
	}

	@Test
	public void testCreateSupplementary() {
		new UnicodeString(SUPPL_STRING);
	}

	@Test
	public void testLength() {
		assertEquals("length of empty string 1", 0, new UnicodeString().length());
		assertEquals("length of empty string 2", 0, new UnicodeString(EMPTY_STRING).length());
		assertEquals("length of non-empty string", 12, new UnicodeString(TEST_STRING).length());
		assertEquals("length of supplementary string", 1, new UnicodeString(SUPPL_STRING).length());
	}

	@Test
	public void testEquals() {
		assertTrue("empty strings are equal 1", new UnicodeString().equals(new UnicodeString()));
		assertTrue("empty strings are equal 2", new UnicodeString().equals(new UnicodeString(EMPTY_STRING)));
		assertTrue("empty strings are equal 3", new UnicodeString(EMPTY_STRING).equals(new UnicodeString()));
		assertTrue("empty strings are equal 4", new UnicodeString(EMPTY_STRING).equals(new UnicodeString(EMPTY_STRING)));
		assertTrue("non-empty strings are equal", new UnicodeString(TEST_STRING).equals(new UnicodeString(TEST_STRING)));
		assertTrue("supplementary strings are equal",
				new UnicodeString(SUPPL_STRING).equals(new UnicodeString(SUPPL_STRING)));
		assertFalse("shouldn't be equal 1", new UnicodeString(EMPTY_STRING).equals(new UnicodeString(TEST_STRING)));
		assertFalse("shouldn't be equal 2", new UnicodeString(EMPTY_STRING).equals(new UnicodeString(SUPPL_STRING)));
		assertFalse("shouldn't be equal 3", new UnicodeString(TEST_STRING).equals(new UnicodeString(EMPTY_STRING)));
		assertFalse("shouldn't be equal 4", new UnicodeString(TEST_STRING).equals(new UnicodeString(SUPPL_STRING)));
		assertFalse("shouldn't be equal 5", new UnicodeString(SUPPL_STRING).equals(new UnicodeString(EMPTY_STRING)));
		assertFalse("shouldn't be equal 6", new UnicodeString(SUPPL_STRING).equals(new UnicodeString(TEST_STRING)));
	}

	@Test
	public void testToString() {
		assertEquals("convert empty string 1", EMPTY_STRING, new UnicodeString().toString());
		assertEquals("convert empty string 2", EMPTY_STRING, new UnicodeString(EMPTY_STRING).toString());
		assertEquals("convert non-empty string", TEST_STRING, new UnicodeString(TEST_STRING).toString());
		assertEquals("convert supplementary string", SUPPL_STRING, new UnicodeString(SUPPL_STRING).toString());
	}

	@Test
	public void testToCase() {
		assertEquals("convert empty string to uppercase", EMPTY_STRING.toUpperCase(), new UnicodeString(EMPTY_STRING)
				.toUpperCase().toString());
		assertEquals("convert non-empty string to uppercase", TEST_STRING.toUpperCase(), new UnicodeString(TEST_STRING)
				.toUpperCase().toString());
		assertEquals("convert supplementary string to uppercase", SUPPL_STRING.toUpperCase(), new UnicodeString(
				SUPPL_STRING).toUpperCase().toString());
		assertEquals("convert empty string to lowercase", EMPTY_STRING.toLowerCase(), new UnicodeString(EMPTY_STRING)
				.toLowerCase().toString());
		assertEquals("convert non-empty string to lowercase", TEST_STRING.toLowerCase(), new UnicodeString(TEST_STRING)
				.toLowerCase().toString());
		assertEquals("convert supplementary string to lowercase", SUPPL_STRING.toLowerCase(), new UnicodeString(
				SUPPL_STRING).toLowerCase().toString());
	}
}
