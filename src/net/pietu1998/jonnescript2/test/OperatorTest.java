package net.pietu1998.jonnescript2.test;

import static org.junit.Assert.assertEquals;
import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.operator.OperatorAdd;
import net.pietu1998.jonnescript2.types.Wrappers;

import org.junit.Test;

public class OperatorTest {

	private static final OperatorAdd add = new OperatorAdd();
	private static final Context dummy = new Context();

	@Test
	public void testAddNull() {
		assertEquals("add null and null", add.apply(dummy, Wrappers.wrapNull(), Wrappers.wrapNull()),
				Wrappers.wrapNull());
	}

	@Test
	public void testAddNumeric() {
		assertEquals("add 12 and 27", add.apply(dummy, Wrappers.wrap(12), Wrappers.wrap(27)), Wrappers.wrap(39));
		assertEquals("add true and false", add.apply(dummy, Wrappers.wrap(true), Wrappers.wrap(false)),
				Wrappers.wrap(1));
		assertEquals("add 38 and true", add.apply(dummy, Wrappers.wrap(38), Wrappers.wrap(true)), Wrappers.wrap(39));
		assertEquals("add 39 and null", add.apply(dummy, Wrappers.wrap(39), Wrappers.wrapNull()), Wrappers.wrap(39));
		assertEquals("add null and false", add.apply(dummy, Wrappers.wrapNull(), Wrappers.wrap(false)),
				Wrappers.wrap(0));
	}

	@Test
	public void testConcatString() {
		assertEquals("concatenate \"a\" and \"b\"", add.apply(dummy, Wrappers.wrap("a"), Wrappers.wrap("b")),
				Wrappers.wrap("ab"));
	}

}
