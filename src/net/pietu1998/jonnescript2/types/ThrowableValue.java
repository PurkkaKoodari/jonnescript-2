package net.pietu1998.jonnescript2.types;

import net.pietu1998.jonnescript2.parser.Node;

public interface ThrowableValue {
	
	void throwException(Node node);

}
