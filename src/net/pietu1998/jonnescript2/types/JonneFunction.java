package net.pietu1998.jonnescript2.types;

import java.util.Optional;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.parser.FormalParameterList;
import net.pietu1998.jonnescript2.parser.NodeBlock;
import net.pietu1998.unicode.UnicodeString;

public class JonneFunction extends JonneFunctionBase {

	private final Context closure;
	private final NodeBlock contents;
	private final FormalParameterList parameterList;

	public JonneFunction(Context closure, FormalParameterList parameterList, NodeBlock contents) {
		this.closure = closure;
		this.parameterList = parameterList;
		this.contents = contents;
	}

	private JonneFunction(JonneObject thisObject, UnicodeString name, Context closure,
			FormalParameterList parameterList, NodeBlock contents) {
		super(thisObject, name);
		this.closure = closure;
		this.parameterList = parameterList;
		this.contents = contents;
	}

	@Override
	public Context wrapContext(Context context) {
		return new Context(context, closure, Optional.ofNullable(getThis()).orElseGet(Wrappers::wrapNull));
	}

	@Override
	protected JonneObject runContents(Context context, JonneObject... args) {
		parameterList.resolve(context, args);
		NodeExecutionResult result = contents.execute(context);
		return result.getReturnValue().orElseGet(Wrappers::wrapNull);
	}

	@Override
	public JonneFunctionBase asMethod(JonneObject thisObject, UnicodeString name) {
		return new JonneFunction(thisObject, name, closure, parameterList, contents);
	}
}
