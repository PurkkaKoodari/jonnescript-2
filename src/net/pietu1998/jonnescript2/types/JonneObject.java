package net.pietu1998.jonnescript2.types;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.operator.Arity;
import net.pietu1998.jonnescript2.operator.BinaryOperator;
import net.pietu1998.jonnescript2.operator.BinaryWriteOperator;
import net.pietu1998.jonnescript2.operator.OperatorType;
import net.pietu1998.jonnescript2.parser.Node;
import net.pietu1998.unicode.UnicodeString;

public class JonneObject {

	public static final UnicodeString STRING_OBJECT = new UnicodeString("olio");

	private final Map<UnicodeString, JonneObject> properties = new HashMap<>();

	protected static final JonneFunctionNative TO_STRING, TO_NUMBER, TO_BOOLEAN, REPR;

	public static class OperatorProperty implements BinaryOperator {
		@Override
		public JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal) {
			if (rightVal instanceof JonneString)
				return leftVal.getPropertyNative(((JonneString) rightVal).value).orElseGet(Wrappers::wrapNull);
			else
				throw new ExecutionException(rightVal.getTypeName() + " ei voi olla ominaisuuden nimi");
		}
	}

	public static class OperatorPropertyWrite implements BinaryWriteOperator {
		@Override
		public void assign(Context context, JonneObject leftVal, JonneObject rightVal, JonneObject assignVal) {
			if (rightVal instanceof JonneString)
				leftVal.setPropertyNative(((JonneString) rightVal).value, assignVal);
			else
				throw new ExecutionException(rightVal.getTypeName() + " ei voi olla ominaisuuden nimi");
		}
	}

	private static final Map<UnicodeString, JonneFunctionNative> nativeMethods = new HashMap<>();

	private static void addNativeMethod(JonneFunctionNative method) {
		nativeMethods.put(method.getOriginalName(), method);
	}

	static {
		TO_STRING = new JonneFunctionNative("tekstiksi", 0, (context, args) -> context.getThis().toJonneStringNative(
				context));
		addNativeMethod(TO_STRING);
		TO_NUMBER = new JonneFunctionNative("luvuksi", 0, (context, args) -> context.getThis().toJonneNumberNative(
				context));
		addNativeMethod(TO_NUMBER);
		TO_BOOLEAN = new JonneFunctionNative("totuusarvoksi", 0,
				(context, args) -> context.getThis().toJonneBooleanNative(context));
		addNativeMethod(TO_BOOLEAN);
		REPR = new JonneFunctionNative("esitys", 0, (context, args) -> context.getThis().toJonneString(context));
		addNativeMethod(REPR);
		for (OperatorType type : OperatorType.values()) {
			if (type.arity.contains(Arity.UNARY) && type.unaryImplementation != null)
				addNativeMethod(new JonneFunctionNative(type.methodName(Arity.UNARY), 0,
						(context, args) -> type.unaryImplementation.apply(context, context.getThis())));
			if (type.arity.contains(Arity.BINARY) && type.binaryImplementation != null)
				addNativeMethod(new JonneFunctionNative(type.methodName(Arity.BINARY), 1,
						(context, args) -> type.binaryImplementation.apply(context, context.getThis(), args[0])));
			if (type.arity.contains(Arity.BINARY) && type.binaryWriteImplementation != null)
				addNativeMethod(new JonneFunctionNative(type.methodName(Arity.BINARY, true), 2, (context, args) -> {
					type.binaryWriteImplementation.assign(context, context.getThis(), args[0], args[1]);
					return Wrappers.wrapNull();
				}));
		}
	}

	public JonneObject() {}

	public final JonneString toJonneString(Context context) {
		return convert(context, TO_STRING.getOriginalName(), JonneString.class);
	}

	public JonneString toJonneStringNative(Context context) {
		return Wrappers.wrap(STRING_OBJECT);
	}

	public final JonneNumber toJonneNumber(Context context) {
		return convert(context, TO_NUMBER.getOriginalName(), JonneNumber.class);
	}

	public JonneNumber toJonneNumberNative(Context context) {
		return Wrappers.wrap(BigDecimal.ZERO);
	}

	public final JonneBoolean toJonneBoolean(Context context) {
		return convert(context, TO_BOOLEAN.getOriginalName(), JonneBoolean.class);
	}

	public JonneBoolean toJonneBooleanNative(Context context) {
		return Wrappers.wrap(true);
	}

	public final JonneString repr(Context context) {
		return (JonneString) callMethod(context, Optional.empty(), REPR.getOriginalName(),
				Optional.of((method, result) -> {
					if (!(result instanceof JonneString))
						throw new ExecutionException(method.getName() + " palautti tyypin " + result.getTypeName());
				}));
	}

	@SuppressWarnings("unchecked")
	private <T extends JonneObject> T convert(Context context, UnicodeString methodName, Class<T> expectedType) {
		return (T) callMethod(context, Optional.empty(), methodName, Optional.of((method, result) -> {
			if (!(result.getClass() == expectedType))
				throw new ExecutionException(method.getName() + " palautti tyypin " + result.getTypeName());
		}));
	}

	protected boolean valueEquals(JonneObject other) {
		return other == this;
	}

	public String getTypeName() {
		return "olio";
	}

	private final static UnicodeString PROPERTY_METHOD = OperatorType.PROPERTY.methodName();
	private final static UnicodeString PROPERTY_WRITE_METHOD = OperatorType.PROPERTY.methodName(true);

	public final Optional<JonneObject> getPropertyOptional(Context context, UnicodeString key) {
		return getPropertyOptional(context, Wrappers.wrap(key));
	}

	public final Optional<JonneObject> getPropertyOptional(Context context, JonneObject key) {
		if (Wrappers.wrap(PROPERTY_METHOD).equals(key))
			return getPropertyNative(PROPERTY_METHOD);
		return Optional.ofNullable(callMethod(context, Optional.empty(), PROPERTY_METHOD, Optional.empty(), key));
	}

	protected final Optional<JonneObject> getPropertyNative(UnicodeString key) {
		if (properties.containsKey(key)) {
			JonneObject value = properties.get(key);
			if (value instanceof JonneFunctionBase)
				return Optional.of(((JonneFunctionBase) value).asMethod(this, key));
			else
				return Optional.of(value);
		} else if (nativeMethods.containsKey(key)) {
			JonneFunctionNative value = nativeMethods.get(key);
			if (value != null)
				return Optional.of(value.asMethod(this, key));
			else
				return Optional.empty();
		} else
			return Optional.empty();
	}

	public final JonneObject getProperty(Context context, UnicodeString key) {
		return getPropertyOptional(context, key).orElseGet(Wrappers::wrapNull);
	}

	public final void setProperty(Context context, UnicodeString key, JonneObject value) {
		callMethod(context, Optional.empty(), PROPERTY_WRITE_METHOD, Optional.empty(), Wrappers.wrap(key), value);
	}

	protected final void setPropertyNative(String key, JonneObject value) {
		setPropertyNative(new UnicodeString(key), value);
	}

	protected final void setPropertyNative(UnicodeString key, JonneObject value) {
		properties.put(key, value);
	}

	private interface ResultValidation {
		void validate(JonneFunctionBase method, JonneObject result);
	}

	public final JonneObject callMethod(Context context, Optional<Node> node, UnicodeString name,
			Optional<ResultValidation> validateResult, JonneObject... args) {
		Optional<JonneObject> method = getPropertyOptional(context, name);
		if (method.isPresent()) {
			JonneObject result, methodFunc = method.get();
			if (methodFunc instanceof JonneFunctionBase) {
				result = ((JonneFunctionBase) methodFunc).run(context, node.orElse(null), args);
			} else
				throw new ExecutionException("tyyppiä " + methodFunc.getTypeName() + " ei voida kutsua");
			validateResult.ifPresent(validate -> validate.validate((JonneFunctionBase) methodFunc, result));
			return result;
		}
		throw new ExecutionException("metodi " + name + " puuttuu");
	}

	@Override
	public final boolean equals(Object obj) {
		return getClass() == obj.getClass() && valueEquals((JonneObject) obj);
	}

}
