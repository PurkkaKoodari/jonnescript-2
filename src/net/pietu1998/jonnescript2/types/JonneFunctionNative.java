package net.pietu1998.jonnescript2.types;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.unicode.UnicodeString;

public class JonneFunctionNative extends JonneFunctionBase {

	public interface NativeFunctionImpl {
		JonneObject run(Context context, JonneObject... args);
	}

	private final NativeFunctionImpl function;
	private final int minArgCount, maxArgCount;
	private final UnicodeString originalName;

	public JonneFunctionNative(String name, int minArgCount, int maxArgCount, NativeFunctionImpl function) {
		this(new UnicodeString(name), minArgCount, maxArgCount, function);
	}

	public JonneFunctionNative(String name, int argCount, NativeFunctionImpl function) {
		this(new UnicodeString(name), argCount, function);
	}

	public JonneFunctionNative(UnicodeString name, int minArgCount, int maxArgCount, NativeFunctionImpl function) {
		super(name);
		this.minArgCount = minArgCount;
		this.maxArgCount = maxArgCount;
		this.function = function;
		this.originalName = name;
	}

	public JonneFunctionNative(UnicodeString name, int argCount, NativeFunctionImpl function) {
		super(name);
		this.minArgCount = this.maxArgCount = argCount;
		this.function = function;
		this.originalName = name;
	}

	private JonneFunctionNative(JonneObject thisObject, UnicodeString name, UnicodeString originalName,
			int minArgCount, int maxArgCount, NativeFunctionImpl function) {
		super(thisObject, name);
		this.minArgCount = minArgCount;
		this.maxArgCount = maxArgCount;
		this.function = function;
		this.originalName = originalName;
	}

	public UnicodeString getOriginalName() {
		return originalName;
	}

	@Override
	public JonneObject runContents(Context context, JonneObject... args) {
		if (minArgCount >= 0 && minArgCount == maxArgCount && args.length != minArgCount) {
			throw new ExecutionException("funktiolle " + getName() + " annettiin " + args.length
					+ " argumenttia, tarvitaan " + minArgCount);
		} else if (args.length < minArgCount) {
			throw new ExecutionException("funktiolle " + getName() + " annettiin " + args.length
					+ " argumenttia, tarvitaan vähintään " + minArgCount);
		} else if (maxArgCount >= 0 && args.length > maxArgCount) {
			throw new ExecutionException("funktiolle " + getName() + " annettiin " + args.length
					+ " argumenttia, tarvitaan enintään " + maxArgCount);
		}
		return function.run(context, args);
	}

	@Override
	public JonneFunctionBase asMethod(JonneObject thisObject, UnicodeString name) {
		return new JonneFunctionNative(thisObject, name, originalName, minArgCount, maxArgCount, function);
	}

}
