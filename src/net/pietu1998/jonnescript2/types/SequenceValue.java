package net.pietu1998.jonnescript2.types;

import java.util.List;

public interface SequenceValue {
	
	JonneObject get(int index);
	
	List<JonneObject> asList();
	
	int length();

}
