package net.pietu1998.jonnescript2.types;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.lexer.NumberParser;

public class JonneNumber extends JonneObject implements NumericValue {

	public final OptimizedNumber value;

	private static final JonneFunctionNative NUMBER_REPR = new JonneFunctionNative(REPR.getOriginalName(), 0,
			(context, args) -> {
				JonneObject thisObject = context.getThis();
				if (!(thisObject instanceof JonneNumber))
					throw new ExecutionException(thisObject.getTypeName() + " ei ole luku");
				return Wrappers.wrap(NumberParser.expressNumber(((JonneNumber) thisObject).value.toBigInteger()));
			});

	{
		setPropertyNative(REPR.getOriginalName(), NUMBER_REPR);
	}

	public JonneNumber(OptimizedNumber value) {
		this.value = value;
	}

	@Override
	public JonneBoolean toJonneBooleanNative(Context context) {
		return Wrappers.wrap(value.signum() != 0);
	}

	@Override
	public JonneNumber toJonneNumberNative(Context context) {
		return Wrappers.wrap(value);
	}

	@Override
	public JonneString toJonneStringNative(Context context) {
		return Wrappers.wrap(value.toString());
	}

	@Override
	protected boolean valueEquals(JonneObject other) {
		return value.compareTo(((JonneNumber) other).value) == 0;
	}

	@Override
	public String getTypeName() {
		return "luku";
	}

}
