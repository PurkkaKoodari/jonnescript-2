package net.pietu1998.jonnescript2.types;

import java.math.BigDecimal;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.unicode.UnicodeString;

public class JonneBoolean extends JonneObject implements NumericValue {

	public static final UnicodeString STRING_TRUE = new UnicodeString("tosi");
	public static final UnicodeString STRING_FALSE = new UnicodeString("kusetus");

	public final boolean value;

	public JonneBoolean(boolean value) {
		this.value = value;
	}

	@Override
	public JonneBoolean toJonneBooleanNative(Context context) {
		return Wrappers.wrap(value);
	}

	@Override
	public JonneNumber toJonneNumberNative(Context context) {
		return Wrappers.wrap(value ? BigDecimal.ONE : BigDecimal.ZERO);
	}

	@Override
	public JonneString toJonneStringNative(Context context) {
		return Wrappers.wrap(value ? STRING_TRUE : STRING_FALSE);
	}

	@Override
	protected boolean valueEquals(JonneObject other) {
		return value == ((JonneBoolean) other).value;
	}

	@Override
	public String getTypeName() {
		return "totuusarvo";
	}

}
