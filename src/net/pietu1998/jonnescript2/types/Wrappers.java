package net.pietu1998.jonnescript2.types;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import net.pietu1998.unicode.UnicodeString;

public final class Wrappers {

	private Wrappers() {}

	public static JonneNull wrapNull() {
		return new JonneNull();
	}

	public static JonneString wrap(String data) {
		if (data == null)
			throw new IllegalArgumentException("data can't be null");
		return new JonneString(data);
	}

	public static JonneString wrap(UnicodeString data) {
		if (data == null)
			throw new IllegalArgumentException("data can't be null");
		return new JonneString(data);
	}

	public static JonneObject wrapNullable(String data) {
		return data == null ? new JonneNull() : new JonneString(data);
	}

	public static JonneObject wrapNullable(UnicodeString data) {
		return data == null ? new JonneNull() : new JonneString(data);
	}

	public static JonneNumber wrap(BigInteger data) {
		if (data == null)
			throw new IllegalArgumentException("data can't be null");
		return new JonneNumber(new OptimizedNumber(data));
	}

	public static JonneNumber wrap(BigDecimal data) {
		if (data == null)
			throw new IllegalArgumentException("data can't be null");
		return new JonneNumber(new OptimizedNumber(data));
	}

	public static JonneNumber wrap(OptimizedNumber data) {
		if (data == null)
			throw new IllegalArgumentException("data can't be null");
		return new JonneNumber(data);
	}

	public static JonneObject wrapNullable(BigInteger data) {
		return data == null ? new JonneNull() : new JonneNumber(new OptimizedNumber(data));
	}

	public static JonneObject wrapNullable(BigDecimal data) {
		return data == null ? new JonneNull() : new JonneNumber(new OptimizedNumber(data));
	}

	public static JonneObject wrapNullable(OptimizedNumber data) {
		return data == null ? new JonneNull() : new JonneNumber(data);
	}

	public static JonneNumber wrap(int data) {
		return new JonneNumber(new OptimizedNumber(BigInteger.valueOf(data)));
	}

	public static JonneNumber wrap(long data) {
		return new JonneNumber(new OptimizedNumber(BigInteger.valueOf(data)));
	}

	public static JonneNumber wrap(float data) {
		return new JonneNumber(new OptimizedNumber(new BigDecimal(data)));
	}

	public static JonneNumber wrap(double data) {
		return new JonneNumber(new OptimizedNumber(new BigDecimal(data)));
	}

	public static JonneBoolean wrap(boolean data) {
		return new JonneBoolean(data);
	}

	public static JonneArray wrap(List<JonneObject> data) {
		if (data == null)
			throw new IllegalArgumentException("data can't be null");
		return new JonneArray(data);
	}

	public static JonneObject wrapNullable(List<JonneObject> data) {
		return data == null ? new JonneNull() : new JonneArray(data);
	}

}
