package net.pietu1998.jonnescript2.types;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.parser.Node;
import net.pietu1998.unicode.UnicodeString;

public class JonneFunctionComposition extends JonneFunctionBase {

	private final JonneFunctionBase outer, inner;

	public JonneFunctionComposition(JonneFunctionBase outer, JonneFunctionBase inner) {
		this.outer = outer;
		this.inner = inner;
	}

	private JonneFunctionComposition(JonneObject thisObject, UnicodeString name, JonneFunctionBase outer,
			JonneFunctionBase inner) {
		super(thisObject, name);
		this.outer = outer;
		this.inner = inner;
	}

	@Override
	public JonneObject run(Context context, Node node, JonneObject... args) {
		// TODO redo
		return outer.run(context, node, inner.run(context, node, args));
	}

	@Override
	protected JonneObject runContents(Context context, JonneObject... args) {
		throw new IllegalStateException("should not be called directly");
	}

	@Override
	public JonneFunctionBase asMethod(JonneObject thisObject, UnicodeString name) {
		return new JonneFunctionComposition(thisObject, name, outer, inner);
	}

}
