package net.pietu1998.jonnescript2.types;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

public class OptimizedNumber extends Number {
	private BigDecimal floatValue;
	private BigInteger intValue;

	private static final MathContext CONTEXT = new MathContext(100);

	public OptimizedNumber(BigInteger intValue) {
		this.intValue = intValue;
	}

	public OptimizedNumber(BigDecimal floatValue) {
		this.floatValue = floatValue;
	}

	public BigDecimal toBigDecimal() {
		if (floatValue != null)
			return floatValue;
		return floatValue = new BigDecimal(intValue);
	}

	public BigInteger toBigInteger() {
		if (intValue != null)
			return intValue;
		return floatValue.toBigInteger();
	}

	public BigInteger toBigIntegerExact() {
		if (intValue != null)
			return intValue;
		return intValue = floatValue.toBigIntegerExact();
	}

	private Number getAsNumber() {
		return intValue != null ? intValue : floatValue;
	}

	public boolean isInteger() {
		return intValue != null;
	}

	private boolean bothInts(OptimizedNumber other) {
		return intValue != null && other.intValue != null;
	}

	public OptimizedNumber negate() {
		return intValue != null ? new OptimizedNumber(intValue.negate()) : new OptimizedNumber(
				floatValue.negate(CONTEXT));
	}

	public OptimizedNumber add(OptimizedNumber other) {
		return bothInts(other) ? new OptimizedNumber(intValue.add(other.intValue)) : new OptimizedNumber(
				toBigDecimal().add(other.toBigDecimal(), CONTEXT));
	}

	public OptimizedNumber subtract(OptimizedNumber other) {
		return bothInts(other) ? new OptimizedNumber(intValue.subtract(other.intValue)) : new OptimizedNumber(
				toBigDecimal().subtract(other.toBigDecimal(), CONTEXT));
	}

	public OptimizedNumber multiply(OptimizedNumber other) {
		return bothInts(other) ? new OptimizedNumber(intValue.multiply(other.intValue)) : new OptimizedNumber(
				toBigDecimal().multiply(other.toBigDecimal(), CONTEXT));
	}

	public OptimizedNumber divide(OptimizedNumber other) {
		if (bothInts(other)) {
			BigInteger[] result = intValue.divideAndRemainder(other.intValue);
			if (result[1].equals(BigInteger.ZERO))
				return new OptimizedNumber(result[0]);
		}
		return new OptimizedNumber(toBigDecimal().divide(other.toBigDecimal(), CONTEXT));
	}

	public OptimizedNumber remainder(OptimizedNumber other) {
		return bothInts(other) ? new OptimizedNumber(intValue.remainder(other.intValue)) : new OptimizedNumber(
				toBigDecimal().remainder(other.toBigDecimal(), CONTEXT));
	}

	public int signum() {
		return intValue != null ? intValue.signum() : floatValue.signum();
	}

	public int compareTo(OptimizedNumber other) {
		return bothInts(other) ? intValue.compareTo(other.intValue) : toBigDecimal().compareTo(other.toBigDecimal());
	}

	public int intValueExact() {
		return intValue != null ? intValue.intValueExact() : floatValue.intValueExact();
	}

	@Override
	public int hashCode() {
		return getAsNumber().hashCode();
	}

	@Override
	public String toString() {
		return getAsNumber().toString();
	}

	@Override
	public int intValue() {
		return getAsNumber().intValue();
	}

	@Override
	public long longValue() {
		return getAsNumber().longValue();
	}

	@Override
	public float floatValue() {
		return getAsNumber().floatValue();
	}

	@Override
	public double doubleValue() {
		return getAsNumber().doubleValue();
	}
}
