package net.pietu1998.jonnescript2.types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;

public class JonneArray extends JonneObject implements AssignableSequenceValue {

	public final ArrayList<JonneObject> data;

	private static final JonneFunctionNative ARRAY_REPR = new JonneFunctionNative(REPR.getOriginalName(), 0,
			(context, args) -> {
				JonneObject thisObject = context.getThis();
				if (!(thisObject instanceof JonneArray))
					throw new ExecutionException(thisObject.getTypeName() + " ei ole lista");
				return Wrappers.wrap("taulukko "
						+ ((JonneArray) thisObject).data.stream().sequential().map(
								item -> item.repr(context).value.toString()).collect(Collectors.joining(" sitten "))
						+ " valmis");
			});

	{
		setPropertyNative(REPR.getOriginalName(), ARRAY_REPR);
	}

	public JonneArray() {
		data = new ArrayList<>();
	}

	public JonneArray(List<JonneObject> data) {
		this.data = new ArrayList<>(data);
	}

	@Override
	public JonneString toJonneStringNative(Context context) {
		return Wrappers.wrap(data.stream().sequential().map(val -> val.toJonneString(context).value.toString()).collect(
				Collectors.joining(",")));
	}

	@Override
	public JonneObject get(int index) {
		return data.get(index);
	}

	@Override
	public void set(int index, JonneObject value) {
		while (index >= data.size())
			data.add(Wrappers.wrapNull());
		data.set(index, value);
	}

	@Override
	public List<JonneObject> asList() {
		return Collections.unmodifiableList(data);
	}

	@Override
	public int length() {
		return data.size();
	}

	@Override
	protected boolean valueEquals(JonneObject other) {
		List<JonneObject> arr = ((JonneArray) other).data;
		if (data.size() != arr.size())
			return false;
		for (int i = 0; i < data.size(); i++) {
			if (!data.get(i).equals(arr.get(i)))
				return false;
		}
		return true;
	}

	@Override
	public String getTypeName() {
		return "lista";
	}

}
