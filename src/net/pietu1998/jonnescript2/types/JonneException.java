package net.pietu1998.jonnescript2.types;

import java.util.ArrayList;
import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.exception.StackTraceLevel;
import net.pietu1998.jonnescript2.parser.Node;
import net.pietu1998.unicode.UnicodeString;

public class JonneException extends JonneObject implements ThrowableValue {

	public static final UnicodeString FIELD_MESSAGE = new UnicodeString("viesti");
	public static final UnicodeString FIELD_STACK = new UnicodeString("pino");
	public static final UnicodeString FIELD_FUNCTION = new UnicodeString("funktio");
	public static final UnicodeString FIELD_FILE = new UnicodeString("tiedosto");
	public static final UnicodeString FIELD_LINE = new UnicodeString("rivi");

	private final ExecutionException exception;

	public JonneException(ExecutionException exception) {
		this.exception = exception;
		setPropertyNative(FIELD_MESSAGE, Wrappers.wrap(exception.getMessage()));
		List<JonneObject> stack = new ArrayList<>();
		for (final StackTraceLevel level : exception.getStack()) {
			JonneObject levelObject = new JonneObject() {
				@Override
				public JonneString toJonneStringNative(Context context) {
					return Wrappers.wrap(level.toString());
				}
			};
			levelObject.setPropertyNative(FIELD_FUNCTION, Wrappers.wrapNullable(level.getFunction()));
			levelObject.setPropertyNative(FIELD_FILE, Wrappers.wrapNullable(level.getFile()));
			levelObject.setPropertyNative(FIELD_LINE,
					level.getLine() == -1 ? Wrappers.wrapNull() : Wrappers.wrap(level.getLine()));
			stack.add(levelObject);
		}
		setPropertyNative(FIELD_STACK, Wrappers.wrap(stack));
	}

	@Override
	public JonneString toJonneStringNative(Context context) {
		return Wrappers.wrap(exception.toString());
	}
	
	@Override
	public void throwException(Node node) {
		throw new ExecutionException(exception.getMessage(), node);
	}

	@Override
	public String getTypeName() {
		return "virhe";
	}

	@Override
	protected boolean valueEquals(JonneObject other) {
		return exception == ((JonneException) other).exception;
	}

}
