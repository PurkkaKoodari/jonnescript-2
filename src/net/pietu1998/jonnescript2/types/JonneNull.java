package net.pietu1998.jonnescript2.types;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.unicode.UnicodeString;

public class JonneNull extends JonneObject implements NumericValue {

	public static final UnicodeString STRING_NULL = new UnicodeString("tyhjä");

	@Override
	public JonneString toJonneStringNative(Context context) {
		return Wrappers.wrap(STRING_NULL);
	}

	@Override
	public JonneBoolean toJonneBooleanNative(Context context) {
		return Wrappers.wrap(false);
	}

	@Override
	protected boolean valueEquals(JonneObject other) {
		return true;
	}

	@Override
	public String getTypeName() {
		return "tyhjä";
	}

}
