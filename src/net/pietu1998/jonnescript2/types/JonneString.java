package net.pietu1998.jonnescript2.types;

import java.io.StringReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.lexer.KeywordType;
import net.pietu1998.jonnescript2.lexer.Lexer;
import net.pietu1998.jonnescript2.lexer.StringToken;
import net.pietu1998.jonnescript2.lexer.Tokenizer;
import net.pietu1998.jonnescript2.parser.Node;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.unicode.UnicodeStringBuilder;

public class JonneString extends JonneObject implements ParseableNumericValue, SequenceValue, ThrowableValue {

	public final UnicodeString value;

	private static final JonneFunctionNative STRING_REPR = new JonneFunctionNative(REPR.getOriginalName(), 0,
			(context, args) -> {
				JonneObject thisObject = context.getThis();
				if (!(thisObject instanceof JonneString))
					throw new ExecutionException(thisObject.getTypeName() + " ei ole teksti");
				JonneString thisString = (JonneString) thisObject;
				try {
					UnicodeStringBuilder builder = new UnicodeStringBuilder();
					builder.append(Lexer.BEGIN_STRING).append(' ');
					Tokenizer tokenizer = new Tokenizer(new StringReader(thisString.value.toString()), null);
					StringToken token = tokenizer.pollToken();
					while (token != null) {
						builder.append(token.padding);
						if (Lexer.isTokenSpecialInString(token))
							builder.append(' ').append(Lexer.ESCAPE).append(' ').append(token.content).append(' ');
						else
							builder.append(token.content);
						token = tokenizer.pollToken();
					}
					builder.append(tokenizer.getCachedPadding(false)).append(' ').append(
							KeywordType.END_BLOCK.sourceString);
					return Wrappers.wrap(builder.toUnicodeString());
				} catch (Exception e) {
					throw new IllegalStateException();
				}
			});

	{
		setPropertyNative(REPR.getOriginalName(), STRING_REPR);
	}

	public JonneString(String value) {
		this.value = new UnicodeString(value);
	}

	public JonneString(UnicodeString value) {
		this.value = value;
	}

	@Override
	public JonneBoolean toJonneBooleanNative(Context context) {
		return Wrappers.wrap(value.length() != 0);
	}

	@Override
	public JonneNumber toJonneNumberNative(Context context) {
		try {
			return Wrappers.wrap(new BigDecimal(value.toString()));
		} catch (NumberFormatException e) {
			return Wrappers.wrap(BigDecimal.ZERO);
		}
	}

	@Override
	public JonneString toJonneStringNative(Context context) {
		return Wrappers.wrap(value);
	}

	@Override
	public JonneObject get(int index) {
		return Wrappers.wrap(UnicodeString.fromData(new int[] { value.codePointAt(index) }));
	}

	@Override
	public List<JonneObject> asList() {
		return value.codePoints().mapToObj(code -> Wrappers.wrap(UnicodeString.fromData(new int[] { (char) code }))).collect(
				Collectors.toList());
	}

	@Override
	public int length() {
		return value.length();
	}

	@Override
	protected boolean valueEquals(JonneObject other) {
		return value.equals(((JonneString) other).value);
	}

	@Override
	public void throwException(Node node) {
		throw new ExecutionException(value.toString(), node);
	}

	@Override
	public String getTypeName() {
		return "teksti";
	}

}
