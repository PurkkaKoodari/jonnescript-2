package net.pietu1998.jonnescript2.types;

public interface AssignableSequenceValue extends SequenceValue {
	
	void set(int index, JonneObject value);

}
