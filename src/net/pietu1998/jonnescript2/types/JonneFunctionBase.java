package net.pietu1998.jonnescript2.types;

import java.util.Optional;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.lexer.KeywordType;
import net.pietu1998.jonnescript2.parser.Node;
import net.pietu1998.unicode.UnicodeString;

public abstract class JonneFunctionBase extends JonneObject {

	private final JonneObject thisObject;
	private final UnicodeString name;

	protected JonneFunctionBase() {
		this(KeywordType.FUNCTION.sourceString);
	}

	protected JonneFunctionBase(UnicodeString name) {
		this(null, name);
	}

	protected JonneFunctionBase(JonneObject thisObject, UnicodeString name) {
		this.thisObject = thisObject;
		this.name = name;
	}

	protected JonneObject getThis() {
		return thisObject;
	}

	public final String getName() {
		return thisObject == null ? name.toString() : thisObject.getTypeName() + "#" + name.toString();
	}

	public final UnicodeString getUnicodeName() {
		return thisObject == null ? name : new UnicodeString(thisObject.getTypeName() + "#").concat(name);
	}

	protected Context wrapContext(Context context) {
		return new Context(context, Optional.ofNullable(thisObject).orElseGet(Wrappers::wrapNull));
	}

	public JonneObject run(Context context, Node node, JonneObject... args) {
		context = wrapContext(context);
		try {
			return runContents(context, args);
		} catch (ExecutionException e) {
			throw e.inside(getName(), node);
		}
	}

	protected abstract JonneObject runContents(Context context, JonneObject... args);

	@Override
	public JonneString toJonneStringNative(Context context) {
		return Wrappers.wrap(KeywordType.FUNCTION.sourceString + " " + getUnicodeName());
	}

	public abstract JonneFunctionBase asMethod(JonneObject thisObject, UnicodeString name);

	@Override
	public String getTypeName() {
		return "toiminto";
	}

}
