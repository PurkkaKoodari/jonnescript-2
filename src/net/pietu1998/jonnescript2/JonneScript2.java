package net.pietu1998.jonnescript2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.exception.ParseException;
import net.pietu1998.jonnescript2.lexer.Lexer;
import net.pietu1998.jonnescript2.lexer.StringToken;
import net.pietu1998.jonnescript2.lexer.Tokenizer;
import net.pietu1998.jonnescript2.parser.Node;
import net.pietu1998.jonnescript2.parser.Parser;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.util.DumpWriter;

public class JonneScript2 {

	public static final String VERSION = "2.0-a7";

	public static void main(String[] args) {
		Optional<DebugType> debugLevel = Optional.empty();
		File file = null;
		int i = 0;
		for (; i < args.length; i++) {
			if (args[i].equals("-h") || args[i].equals("--help")) {
				System.out.println("käyttö: jonnescript2 [valinnat] [tiedosto]");
				System.out.println();
				System.out.println("  -h, --help          näyttää tämän viestin");
				System.out.println("  -V, --version       tulostaa JonneScriptin version");
				System.out.println("      --debug=tyyppi  asettaa debug-tyypin");
				System.exit(0);
			} else if (args[i].equals("-V") || args[i].equals("--version")) {
				System.out.println("JonneScript " + VERSION);
				System.exit(0);
			} else if (args[i].startsWith("-debug=")) {
				if (debugLevel.isPresent()) {
					System.err.println("debug-tyyppi annettu kahdesti");
					System.exit(1);
				}
				String typeName = args[i].substring(7);
				debugLevel = Stream.of(DebugType.values()).filter(type -> type.name().toLowerCase().equals(typeName)).findFirst();
				if (!debugLevel.isPresent()) {
					System.err.println("virheellinen debug-tyyppi \"" + args[i].substring(7) + "\"");
					System.err.println("debug-tyypit: "
							+ Stream.of(DebugType.values()).map(type -> type.name().toLowerCase()).collect(
									Collectors.joining(", ")));
					System.exit(1);
				}
			} else if (args[i].equals("--")) {
				if (i < args.length - 1)
					file = new File(args[++i]);
				break;
			} else if (args[i].startsWith("-")) {
				System.err.println("tuntematon valitsin \"" + args[i] + "\"");
				System.exit(1);
			} else {
				file = new File(args[i]);
				break;
			}
		}
		DebugType debugType = debugLevel.orElse(DebugType.NONE);
		List<String> programArgs;
		if (i < args.length - 1)
			programArgs = Arrays.asList(Arrays.copyOfRange(args, i + 1, args.length));
		else
			programArgs = Collections.emptyList();
		Context context = new Context();
		context.setVariable("argumentit",
				Wrappers.wrap(programArgs.stream().map(Wrappers::wrap).collect(Collectors.toList())));
		if (file == null) {
			if (debugType != DebugType.NONE && debugType != DebugType.ERRORS) {
				System.err.println("debug-tyyppi " + debugType.name().toLowerCase() + " ei toimi REPL-ympäristössä");
				System.exit(1);
			}
			runRepl(context, debugType);
		} else {
			runFromFile(file, context, debugType);
		}
	}

	private static void runFromFile(File file, Context context, DebugType debugLevel) {
		try {
			Reader reader = new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8"));
			Tokenizer tokenizer = new Tokenizer(reader, file.getName());
			if (debugLevel == DebugType.TOKENIZER) {
				dumpStringTokens(tokenizer);
			} else {
				Lexer lexer = new Lexer(tokenizer);
				if (debugLevel == DebugType.LEXER) {
					dumpTokens(lexer);
				} else {
					Parser parser = new Parser(lexer);
					Node parsed = parser.parseBlock();
					if (debugLevel == DebugType.PARSER) {
						dumpTree(parsed);
					} else {
						doInterpret(context, parsed);
					}
				}
			}
			reader.close();
		} catch (ParseException e) {
			if (debugLevel == DebugType.ERRORS)
				e.printStackTrace();
			else
				System.err.println(e.toString());
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println("tiedostoa ei löydy");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("tiedostoa ei voitu lukea");
			System.exit(1);
		}
	}

	private static void runRepl(Context context, DebugType debugLevel) {
		Scanner scanner = new Scanner(System.in);
		Tokenizer tokenizer = new Tokenizer(new StringReader(""), "<repl>");
		Lexer lexer = new Lexer(tokenizer, () -> {
			System.out.print("... ");
			if (scanner.hasNextLine()) {
				String line = scanner.nextLine() + "\n";
				tokenizer.setReader(new StringReader(line));
			}
		});
		try {
			Parser parser = new Parser(lexer);
			while (true) {
				try {
					tokenizer.resetCache();
					tokenizer.resetLine();
					System.out.print(">>> ");
					if (!scanner.hasNextLine()) {
						System.out.println();
						break;
					}
					String line = scanner.nextLine() + "\n";
					tokenizer.setReader(new StringReader(line));
					lexer.next();
					Node parsed = parser.parseBlock();
					Optional<JonneObject> result = parsed.execute(context).result;
					if (result.isPresent() && !(result.get() instanceof JonneNull))
						System.out.println(result.get().toJonneString(context).value.toString());
				} catch (ParseException e) {
					if (debugLevel == DebugType.ERRORS)
						e.printStackTrace();
					else
						System.err.println(e.toString());
				} catch (ExecutionException e) {
					if (debugLevel == DebugType.ERRORS)
						e.printStackTrace();
					else
						System.err.println(e.toString());
				} catch (IOException e) {
					e.printStackTrace();
					break;
				} catch (NoSuchElementException e) {
					break;
				}
			}
		} catch (IOException | ParseException e) {
			System.err.println("repl-ympäristön alustus epäonnistui");
		} finally {
			scanner.close();
		}
	}

	private static void doInterpret(Context context, Node parsed) {
		try {
			parsed.execute(context);
		} catch (ExecutionException e) {
			System.err.println(e.toString());
		}
	}

	private static void dumpStringTokens(Tokenizer tokenizer) throws IOException {
		System.out.println("Dumping tokenizer output.");
		StringToken token = tokenizer.pollToken();
		while (token != null) {
			System.out.println(token);
			token = tokenizer.pollToken();
		}
	}

	private static void dumpTokens(Lexer lexer) throws IOException, ParseException {
		System.out.println("Dumping lexer output.");
		lexer.next();
		while (lexer.hasToken()) {
			System.out.println(lexer.peek().orElseThrow(AssertionError::new));
			lexer.next();
		}
	}

	private static void dumpTree(Node parsed) {
		System.out.println("Dumping parser output.");
		parsed.dump(new DumpWriter(System.out));
	}
}
