package net.pietu1998.jonnescript2;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * A frame that displays a full-screen image of a Blue Screen of Death.
 */
public class BsodFrame extends JFrame {

	private static BufferedImage bsod;

	/**
	 * Creates and shows a Blue Screen of Death frame.
	 * 
	 * @throws HeadlessException
	 *             if {@code GraphicsEnvironment.isHeadless()} returns
	 *             {@code true}.
	 */
	public BsodFrame() throws HeadlessException {
		super();
		try {
			bsod = ImageIO.read(BsodFrame.class.getResource("/bsod.png"));
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			setUndecorated(true);
			try {
				setAlwaysOnTop(true);
			} catch (SecurityException e) {}
			add(new BSODCanvas());
			setVisible(true);
			setExtendedState(MAXIMIZED_BOTH);
			Image cursorImage = Toolkit.getDefaultToolkit().createImage(BsodFrame.class.getResource("/blank.png"));
			final Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage, new Point(0, 0), "blank");
			addMouseMotionListener(new MouseMotionAdapter() {
				@Override
				public void mouseMoved(MouseEvent e) {
					setCursor(cursor);
				}
			});
		} catch (IOException e) {}
	}

	private static class BSODCanvas extends JComponent {
		@Override
		public void paint(Graphics g) {
			g.drawImage(bsod, 0, 0, getWidth(), getHeight(), 0, 0, bsod.getWidth(), bsod.getHeight(), null);
		}
	}

}
