package net.pietu1998.jonnescript2;

import java.util.HashMap;
import java.util.Optional;

import net.pietu1998.jonnescript2.exception.VariableNotFoundException;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.unicode.UnicodeString;

/**
 * <p>
 * Represents a context in which code can be executed.
 * </p>
 * <p>
 * The execution context contains all the variables used by the program.
 * </p>
 */
public class Context {

	private final Context parent;
	private final Context closure;
	private final JonneObject thisObject;
	private final HashMap<UnicodeString, JonneObject> variables = new HashMap<>();

	/**
	 * Creates a root {@code ExecutionContext} and adds the standard functions
	 * to it.
	 */
	public Context() {
		this(null, null, Wrappers.wrapNull());
		addStandardFunctions();
	}

	/**
	 * Creates a {@code ExecutionContext} with the given parent.
	 * 
	 * @param parent
	 *            the parent of the new context.
	 */
	public Context(Context parent) {
		this(parent, null, Wrappers.wrapNull());
	}

	/**
	 * Creates a {@code ExecutionContext} with the given parent and this-object.
	 * 
	 * @param parent
	 *            the parent of the new context.
	 * @param thisObject
	 *            the this-object of the new context.
	 */
	public Context(Context parent, JonneObject thisObject) {
		this(parent, null, thisObject);
	}

	/**
	 * Creates a {@code ExecutionContext} with the given parent, closure and
	 * this-object.
	 * 
	 * @param parent
	 *            the parent of the new context.
	 * @param closure
	 *            the closure of the new context.
	 * @param thisObject
	 *            the this-object of the new context.
	 */
	public Context(Context parent, Context closure, JonneObject thisObject) {
		if (thisObject == null)
			throw new IllegalArgumentException("thisObject can't be null");
		this.parent = parent;
		this.closure = closure;
		this.thisObject = thisObject;
	}

	/**
	 * Gets a variable from this context, or if it isn't found in this context,
	 * any ancestors this context may have.
	 * 
	 * @param name
	 *            the name of the variable to get.
	 * @return The value of the variable.
	 * @throws VariableNotFoundException
	 *             if the variable is not found in this context or its
	 *             ancestors.
	 */
	public JonneObject getVariable(UnicodeString name) throws VariableNotFoundException {
		return getVariableOptional(name).orElseThrow(() -> new VariableNotFoundException(name));
	}

	private Optional<JonneObject> getVariableOptional(UnicodeString name) {
		if (variables.containsKey(name))
			return Optional.of(variables.get(name));
		if (closure != null) {
			Optional<JonneObject> value = closure.getVariableOptional(name);
			if (value.isPresent())
				return value;
		}
		if (parent != null) {
			Optional<JonneObject> value = parent.getVariableOptional(name);
			if (value.isPresent())
				return value;
		}
		return Optional.empty();
	}

	/**
	 * Gets a variable from this context, or if it isn't found in this context,
	 * any ancestors this context may have. The name of the variable is first
	 * decoded from UTF-16.
	 * 
	 * @param name
	 *            the name of the variable to get.
	 * @return The value of the variable.
	 * @throws VariableNotFoundException
	 *             if the variable is not found in this context or its
	 *             ancestors.
	 */
	public JonneObject getVariable(String name) throws VariableNotFoundException {
		return getVariable(new UnicodeString(name));
	}

	/**
	 * Sets a variable in this context.
	 * 
	 * @param name
	 *            the name of the variable to set.
	 * @param value
	 *            the new value of the variable.
	 */
	public void setVariable(UnicodeString name, JonneObject value) {
		variables.put(name, value);
	}

	/**
	 * Sets a variable in this context. The name of the variable is first
	 * decoded from UTF-16.
	 * 
	 * @param name
	 *            the name of the variable to set.
	 * @param value
	 *            the new value of the variable.
	 */
	public void setVariable(String name, JonneObject value) {
		setVariable(new UnicodeString(name), value);
	}

	/**
	 * Gets the this-object of this context.
	 * 
	 * @return The this-object of this context.
	 */
	public JonneObject getThis() {
		return thisObject;
	}

	private void addStandardFunctions() {
		setVariable("tulosta", NativeFunctions.print);
		setVariable("lue", NativeFunctions.input);
		setVariable("poistu", NativeFunctions.exit);
		setVariable("versio", Wrappers.wrap(JonneScript2.VERSION));
	}
}
