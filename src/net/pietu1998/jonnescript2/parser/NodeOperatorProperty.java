package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.util.DumpWriter;

public class NodeOperatorProperty extends EvaluableNode implements AssignableNode {

	private final EvaluableNode left;
	private final PropertyNameNode right;

	public NodeOperatorProperty(Token location, EvaluableNode left, PropertyNameNode right) {
		super(location);
		this.left = left;
		this.right = right;
	}
	
	@Override
	public boolean isAssignable() {
		return true;
	}

	@Override
	public void assign(JonneObject value, Context context) {
		left.evaluate(context).setProperty(context, right.getPropertyName(context), value);
	}

	@Override
	public JonneObject evaluate(Context context) {
		return left.evaluate(context).getProperty(context, right.getPropertyName(context));
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("property operator");
		writer.dumpIndented(left);
		writer.dumpIndented(right);
	}

}
