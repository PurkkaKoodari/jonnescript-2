package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneException;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.util.DumpWriter;

public class NodeTry extends Node {

	private final UnicodeString catchVariable;
	private final NodeBlock tryBlock, catchBlock;

	public NodeTry(Token location, UnicodeString catchVariable, NodeBlock tryBlock, NodeBlock catchBlock) {
		super(location);
		this.catchVariable = catchVariable;
		this.tryBlock = tryBlock;
		this.catchBlock = catchBlock;
	}

	@Override
	public NodeExecutionResult execute(Context context) {
		NodeExecutionResult result = NodeExecutionResult.empty();
		try {
			NodeExecutionResult newResult = tryBlock.execute(context);
			if (newResult.result.isPresent()) {
				result = newResult;
				if (newResult.returned)
					return newResult;
			}
		} catch (ExecutionException e) {
			context.setVariable(catchVariable, new JonneException(e));
			NodeExecutionResult newResult = catchBlock.execute(context);
			if (newResult.result.isPresent())
				result = newResult;
		}
		return result;
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.print("try (");
		writer.print(catchVariable.toString());
		writer.println(")");
		writer.dumpIndented(tryBlock);
		writer.dumpIndented(catchBlock);
	}

}
