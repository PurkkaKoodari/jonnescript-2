package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.util.DumpWriter;

public class NodeThis extends EvaluableNode {

	public NodeThis(Token location) {
		super(location);
	}

	@Override
	public JonneObject evaluate(Context context) {
		return context.getThis();
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("this");
	}

}
