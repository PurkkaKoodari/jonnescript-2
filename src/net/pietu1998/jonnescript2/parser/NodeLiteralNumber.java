package net.pietu1998.jonnescript2.parser;

import java.math.BigDecimal;
import java.math.BigInteger;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.OptimizedNumber;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.util.DumpWriter;

public class NodeLiteralNumber extends EvaluableNode {

	private final OptimizedNumber value;

	public NodeLiteralNumber(Token location, BigDecimal value) {
		super(location);
		this.value = new OptimizedNumber(value);
	}

	public NodeLiteralNumber(Token location, BigInteger value) {
		super(location);
		this.value = new OptimizedNumber(value);
	}

	@Override
	public JonneObject evaluate(Context context) {
		return Wrappers.wrap(value);
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.print("number literal (");
		writer.print(value.toString());
		writer.println(")");
	}

}
