package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.exception.VariableNotFoundException;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.util.DumpWriter;

public class NodeIdentifier extends EvaluableNode implements AssignableNode, PropertyNameNode {

	private final UnicodeString name;

	public NodeIdentifier(Token location, UnicodeString name) {
		super(location);
		this.name = name;
	}

	@Override
	public JonneObject evaluate(Context context) {
		try {
			return context.getVariable(name);
		} catch (VariableNotFoundException e) {
			throw new ExecutionException("muuttujaa " + e.getVariableName() + " ei ole määritelty", this);
		}
	}

	@Override
	public void assign(JonneObject value, Context context) {
		context.setVariable(name, value);
	}
	
	@Override
	public boolean isAssignable() {
		return true;
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.print("identifier (");
		writer.print(name.toString());
		writer.println(")");
	}

	@Override
	public UnicodeString getPropertyName(Context context) {
		return name;
	}

}
