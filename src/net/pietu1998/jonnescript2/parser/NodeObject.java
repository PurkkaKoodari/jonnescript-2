package net.pietu1998.jonnescript2.parser;

import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.util.DumpWriter;
import net.pietu1998.util.Pair;

public class NodeObject extends EvaluableNode {

	private final List<Pair<PropertyNameNode, EvaluableNode>> elements;

	public NodeObject(Token location, List<Pair<PropertyNameNode, EvaluableNode>> elements) {
		super(location);
		this.elements = elements;
	}

	@Override
	public JonneObject evaluate(Context context) {
		JonneObject object = new JonneObject();
		elements.forEach(item -> {
			UnicodeString name = item.getFirst().getPropertyName(context);
			object.setProperty(context, name, item.getSecond().evaluate(context));
		});
		return object;
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("object");
		writer.indent();
		for (Pair<PropertyNameNode, EvaluableNode> element : elements) {
			writer.dump(element.getFirst());
			writer.dumpIndented(element.getSecond());
		}
		writer.dedent();
	}

}
