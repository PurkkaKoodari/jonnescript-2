package net.pietu1998.jonnescript2.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import net.pietu1998.jonnescript2.exception.ParseException;
import net.pietu1998.jonnescript2.lexer.KeywordType;
import net.pietu1998.jonnescript2.lexer.Lexer;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.lexer.TokenIdentifier;
import net.pietu1998.jonnescript2.lexer.TokenKeyword;
import net.pietu1998.jonnescript2.lexer.TokenLiteralBoolean;
import net.pietu1998.jonnescript2.lexer.TokenLiteralNull;
import net.pietu1998.jonnescript2.lexer.TokenLiteralNumber;
import net.pietu1998.jonnescript2.lexer.TokenLiteralString;
import net.pietu1998.jonnescript2.lexer.TokenOperator;
import net.pietu1998.jonnescript2.lexer.TokenOperatorName;
import net.pietu1998.jonnescript2.operator.Arity;
import net.pietu1998.jonnescript2.operator.OperatorType;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.util.Pair;

/**
 * Takes a stream of {@link Token}s provided by a {@link Lexer} and converts it
 * into a {@link Node} tree as defined by JonneScript 2.
 */
public class Parser {

	private Lexer lexer;

	/**
	 * Creates a {@code Parser} with the given lexer, and advances the lexer
	 * using {@link Lexer#next()}.
	 * 
	 * @param lexer
	 *            the {@link Lexer} to poll tokens from.
	 * @throws IOException
	 *             if {@link Lexer#next()} throws an {@code IOException}.
	 * @throws ParseException
	 *             if {@link Lexer#next()} throws a {code ParseException}.
	 */
	public Parser(Lexer lexer) throws IOException, ParseException {
		this.lexer = lexer;
		this.lexer.next();
	}

	private Token peekRequiredToken(Supplier<ParseException> error) throws IOException, ParseException {
		Optional<Token> token = lexer.peek();
		if (!token.isPresent()) {
			lexer.requestInputContinuation();
			lexer.next();
			token = lexer.peek();
		}
		return token.orElseThrow(error);
	}

	/**
	 * <p>
	 * Parses a plain code block.
	 * </p>
	 * <p>
	 * This method reads all available tokens from the {@link Lexer}, and
	 * creates a {@link NodeBlock} node out of the statements.
	 * </p>
	 * <p>
	 * If the lexer runs out of tokens and the parser is in a state where an
	 * end-of-file is not valid, the parser may request more input via the
	 * lexer, which may cause this method to block.
	 * </p>
	 * 
	 * @return The parsed code block.
	 * @throws IOException
	 *             if the underlying {@link Lexer} throws an {@code IOException}
	 *             .
	 * @throws ParseException
	 *             if a syntax error is encountered, or the underlying
	 *             {@link Lexer} throws a {@code ParseException}.
	 * @see Lexer#next()
	 * @see Lexer.InputContinuationRequest
	 */
	public NodeBlock parseBlock() throws IOException, ParseException {
		return parseBlock(false, false, false, false);
	}

	private NodeBlock parseBlock(boolean requireClose, boolean allowReturn) throws IOException, ParseException {
		return parseBlock(requireClose, allowReturn, false, false);
	}

	private NodeBlock parseBlock(boolean requireClose, boolean allowReturn, boolean allowElse, boolean allowCatch)
			throws IOException, ParseException {
		List<Node> statements = new ArrayList<>();
		if (requireClose) {
			Token token = peekRequiredToken(() -> new ParseException("sulkematon koodilohko"));
			while (!endBlockIfNeeded(token, allowElse, allowCatch)) {
				Node statement = parseStatement(allowReturn);
				if (statement != null)
					statements.add(statement);
				token = peekRequiredToken(() -> new ParseException("sulkematon koodilohko"));
			}
		} else {
			while (lexer.hasToken()) {
				Token token = lexer.peek().orElseThrow(AssertionError::new);
				if (token.equals(new TokenKeyword(KeywordType.END_BLOCK)))
					throw new ParseException(token.getFriendlyName() + " koodilohkon ulkopuolella", token);
				Node statement = parseStatement(allowReturn);
				if (statement != null)
					statements.add(statement);
			}
			lexer.next();
		}
		return new NodeBlock(statements);
	}

	private boolean endBlockIfNeeded(Token token, boolean allowElse, boolean allowCatch) throws IOException,
			ParseException {
		if (advanceIf(token.equals(new TokenKeyword(KeywordType.END_BLOCK))))
			return true;
		if (token.equals(new TokenKeyword(KeywordType.ELSE))) {
			if (allowElse)
				return true;
			else
				throw new ParseException(token.getFriendlyName() + " " + KeywordType.IF.sourceString
						+ "-lauseen ulkopuolella");
		}
		if (token.equals(new TokenKeyword(KeywordType.CATCH))) {
			if (allowCatch)
				return true;
			else
				throw new ParseException(token.getFriendlyName() + " " + KeywordType.TRY.sourceString
						+ "-lauseen ulkopuolella");
		}
		return false;
	}

	/**
	 * <p>
	 * Parses a single statement.
	 * </p>
	 * <p>
	 * If the lexer runs out of tokens and the parser is in a state where an
	 * end-of-file is not valid, the parser may request more input via the
	 * lexer, which may cause this method to block.
	 * </p>
	 * 
	 * @param allowReturn
	 *            whether or not to allow a return statement. If this parameter
	 *            is {@code false} and a return statement is found, a
	 *            {@link ParseException} will be thrown.
	 * @return The parsed statement.
	 * @throws IOException
	 *             if the underlying {@link Lexer} throws an {@code IOException}
	 *             .
	 * @throws ParseException
	 *             if a syntax error is encountered, or the underlying
	 *             {@link Lexer} throws a {@code ParseException}.
	 * @see Lexer#next()
	 * @see Lexer.InputContinuationRequest
	 */
	public Node parseStatement(boolean allowReturn) throws IOException, ParseException {
		if (!lexer.hasToken())
			return null;
		Token token = lexer.peek().orElse(null);
		if (token instanceof TokenKeyword) {
			TokenKeyword keywordToken = (TokenKeyword) token;
			switch (keywordToken.keywordType) {
			case COMMENT:
				skipComment(token);
				return null;
			case RETURN:
				lexer.next();
				if (!allowReturn)
					throw new ParseException(token.getFriendlyName() + " funktion ulkopuolella");
				return new NodeReturn(token, parseExpression(token, false, false));
			case IF:
				lexer.next();
				return parseIf(token, allowReturn);
			case THROW:
				lexer.next();
				return parseThrow(token);
			case ELSE:
				break;
			case TRY:
				lexer.next();
				return parseTry(token, allowReturn);
			case CATCH:
				break;
			case UNTIL:
			case WHILE:
				lexer.next();
				return parseConditionLoop(keywordToken, allowReturn);
			case WILLIAMS_BROTHER:
				lexer.next();
				return new NodeWilliamsBrother(token);
			case MOPED:
				lexer.next();
				return new NodeMoped(token);
			case CORIANDER:
				lexer.next();
				throw new ParseException(token.getFriendlyName() + " ei ole toteutettu", token);
			default:
				break;
			}
		}
		return parseExpression(null, false, false);
	}

	private Node parseIf(Token ifToken, boolean allowReturn) throws IOException, ParseException {
		EvaluableNode condition = parseExpression(ifToken, false, false);
		NodeBlock thenBlock = parseBlock(true, allowReturn, true, false);
		Token token = lexer.peek().orElse(null);
		if (advanceIf(new TokenKeyword(KeywordType.ELSE).equals(token)))
			return new NodeIf(ifToken, condition, thenBlock, parseBlock(true, allowReturn));
		else
			return new NodeIf(ifToken, condition, thenBlock, null);
	}

	private Node parseTry(Token tryToken, boolean allowReturn) throws IOException, ParseException {
		NodeBlock tryBlock = parseBlock(true, allowReturn, false, true);
		Token token = lexer.peek().orElse(null);
		if (advanceIf(new TokenKeyword(KeywordType.CATCH).equals(token))) {
			token = peekRequiredToken(() -> new ParseException("virhemuuttujan nimi puuttuu"));
			if (!(token instanceof TokenIdentifier))
				throw new ParseException(token.getFriendlyName() + " nimen tilalla", token);
			lexer.next();
			return new NodeTry(tryToken, ((TokenIdentifier) token).name, tryBlock, parseBlock(true, allowReturn));
		} else
			throw new ParseException(KeywordType.CATCH.sourceString + "-lause puuttuu", tryToken);
	}

	private Node parseThrow(Token throwToken) throws IOException, ParseException {
		EvaluableNode exception = parseExpression(throwToken, false, false);
		return new NodeThrow(throwToken, exception);
	}

	private Node parseConditionLoop(TokenKeyword loopToken, boolean allowReturn) throws IOException, ParseException {
		EvaluableNode condition = parseExpression(loopToken, false, false);
		NodeBlock code = parseBlock(true, allowReturn);
		return new NodeConditionLoop(loopToken, condition, code, loopToken.keywordType == KeywordType.UNTIL);
	}

	private EvaluableNode parseExpression(Token previousToken, boolean allowOperatorName, boolean parenthesis)
			throws IOException, ParseException {
		EvaluableNode left = parseSimpleExpression(allowOperatorName);
		left = parseExpressionRecursive(left, 0);
		if (parenthesis) {
			Token token = peekRequiredToken(() -> new ParseException("sulkemattomat sulkeet"));
			if (!(token instanceof TokenKeyword) || ((TokenKeyword) token).keywordType != KeywordType.END_PARAM_LIST)
				throw new ParseException("sulkemattomat sulkeet");
			lexer.next();
		}
		return left;
	}

	private EvaluableNode parseExpressionRecursive(EvaluableNode left, int minPrecedence) throws ParseException,
			IOException {
		Token token = lexer.peek().orElse(null);
		while (advanceIf(isHigherOrEqualPrecedenceBinaryOperator(token, minPrecedence))) {
			OperatorType operation = ((TokenOperator) token).operatorType;
			if (operation == OperatorType.CALL) {
				List<ArgumentNode> arguments = parseActualParameterList(token);
				left = new NodeOperatorCall(token, left, arguments);
			} else {
				EvaluableNode right = parseSimpleExpression(operation == OperatorType.PROPERTY);
				Token lookahead = lexer.peek().orElse(null);
				while (isHigherPrecedenceBinaryOperator(lookahead, operation.binaryPrecedence)) {
					right = parseExpressionRecursive(right, ((TokenOperator) lookahead).operatorType.binaryPrecedence);
					lookahead = lexer.peek().orElse(null);
				}
				if (operation == OperatorType.ASSIGN) {
					if (left instanceof AssignableNode && ((AssignableNode) left).isAssignable())
						left = new NodeOperatorAssign(token, (AssignableNode) left, right);
					else
						throw new ParseException("lausekkeeseen ei voida sijoittaa", token);
				} else if (operation == OperatorType.PROPERTY) {
					left = new NodeOperatorProperty(token, left, right);
				} else {
					left = new NodeOperatorBinary(token, operation, left, right);
				}
			}
			token = lexer.peek().orElse(null);
		}
		return left;
	}

	private boolean isHigherOrEqualPrecedenceBinaryOperator(Token token, int precedence) {
		if (!(token instanceof TokenOperator))
			return false;
		OperatorType type = ((TokenOperator) token).operatorType;
		return type.arity.contains(Arity.BINARY) && type.binaryPrecedence >= precedence;
	}

	private boolean isHigherPrecedenceBinaryOperator(Token token, int precedence) {
		if (!(token instanceof TokenOperator))
			return false;
		OperatorType type = ((TokenOperator) token).operatorType;
		return type.arity.contains(Arity.BINARY) && (type.binaryPrecedence > precedence);
	}

	private EvaluableNode parseSimpleExpression(boolean allowOperatorName) throws IOException, ParseException {
		Token token = peekRequiredToken(() -> new ParseException("lauseke puuttuu"));
		if (advanceIf(token instanceof TokenIdentifier)) {
			return new NodeIdentifier(token, ((TokenIdentifier) token).name);
		} else if (advanceIf(token instanceof TokenLiteralNull)) {
			return new NodeLiteralNull(token);
		} else if (advanceIf(token instanceof TokenLiteralNumber)) {
			return new NodeLiteralNumber(token, ((TokenLiteralNumber) token).value);
		} else if (advanceIf(token instanceof TokenLiteralString)) {
			return new NodeLiteralString(token, ((TokenLiteralString) token).value);
		} else if (advanceIf(token instanceof TokenLiteralBoolean)) {
			return new NodeLiteralBoolean(token, ((TokenLiteralBoolean) token).value);
		} else if (token instanceof TokenKeyword) {
			switch (((TokenKeyword) token).keywordType) {
			case FUNCTION:
				lexer.next();
				FormalParameterList parameterList = parseFormalParameterList(token);
				NodeBlock code = parseBlock(true, true);
				return new NodeFunction(token, parameterList, code);
			case ARRAY:
				lexer.next();
				return new NodeArray(token, parseActualParameterList(token));
			case OBJECT:
				lexer.next();
				return new NodeObject(token, parseObjectLiteral(token));
			case THIS:
				lexer.next();
				return new NodeThis(token);
			case PARENTHESIS:
				lexer.next();
				return parseExpression(token, false, true);
			default:
				break;
			}
		} else if (token instanceof TokenOperator) {
			OperatorType type = ((TokenOperator) token).operatorType;
			if (!type.arity.contains(Arity.UNARY))
				throw new ParseException(type.sourceString + " ei ole unaarioperaattori", token);
			lexer.next();
			return new NodeOperatorUnary(token, type, parseSimpleExpression(false));
		} else if (advanceIf(token instanceof TokenOperatorName && allowOperatorName)) {
			return new NodeIdentifier(token, ((TokenOperatorName) token).name);
		}
		throw new ParseException(token.getFriendlyName() + " lausekkeen paikalla", token);
	}

	private FormalParameterList parseFormalParameterList(Token owner) throws IOException, ParseException {
		List<UnicodeString> result = new ArrayList<>();
		int varargPosition = -1;
		Token token = peekRequiredToken(() -> new ParseException("sulkematon parametrilista"));
		while (true) {
			if (advanceIf(token.equals(new TokenKeyword(KeywordType.END_PARAM_LIST))))
				break;
			if (advanceIf(token.equals(new TokenKeyword(KeywordType.PARAM_SEPARATOR))))
				token = peekRequiredToken(() -> new ParseException("sulkematon parametrilista"));
			if (!(token instanceof TokenIdentifier))
				throw new ParseException(token.getFriendlyName() + " nimen tilalla", token);
			result.add(((TokenIdentifier) token).name);
			lexer.next();
			token = peekRequiredToken(() -> new ParseException("sulkematon parametrilista"));
			if (advanceIf(token.equals(new TokenKeyword(KeywordType.VARARGS)))) {
				if (varargPosition != -1)
					throw new ParseException("funktiolla saa olla vain yksi vaihtelevapituinen argumentti", token);
				varargPosition = result.size() - 1;
				token = peekRequiredToken(() -> new ParseException("sulkematon parametrilista"));
			}
		}
		return new FormalParameterList(result.toArray(new UnicodeString[0]), varargPosition);
	}

	private List<ArgumentNode> parseActualParameterList(Token owner) throws IOException, ParseException {
		List<ArgumentNode> result = new ArrayList<>();
		Token token = peekRequiredToken(() -> new ParseException("sulkematon parametrilista"));
		while (true) {
			if (advanceIf(token.equals(new TokenKeyword(KeywordType.END_PARAM_LIST))))
				break;
			if (advanceIf(token.equals(new TokenKeyword(KeywordType.PARAM_SEPARATOR))))
				token = peekRequiredToken(() -> new ParseException("sulkematon parametrilista"));
			EvaluableNode node = parseExpression(token, false, false);
			token = peekRequiredToken(() -> new ParseException("sulkematon parametrilista"));
			if (advanceIf(token.equals(new TokenKeyword(KeywordType.VARARGS)))) {
				result.add(new NodeArgumentSpread(token, node));
				token = peekRequiredToken(() -> new ParseException("sulkematon parametrilista"));
			} else
				result.add(node);
		}
		return result;
	}

	private List<Pair<PropertyNameNode, EvaluableNode>> parseObjectLiteral(Token owner) throws IOException,
			ParseException {
		List<Pair<PropertyNameNode, EvaluableNode>> result = new ArrayList<>();
		while (true) {
			Token token = peekRequiredToken(() -> new ParseException("sulkematon olio"));
			if (advanceIf(token.equals(new TokenKeyword(KeywordType.END_PARAM_LIST))))
				break;
			if (advanceIf(token.equals(new TokenKeyword(KeywordType.PARAM_SEPARATOR))))
				token = peekRequiredToken(() -> new ParseException("sulkematon olio"));
			PropertyNameNode node = parseExpression(owner, true, false);
			result.add(Pair.of(node, parseExpression(token, false, false)));
		}
		return result;
	}

	private void skipComment(Token start) throws IOException, ParseException {
		Token token = peekRequiredToken(() -> new ParseException("sulkematon kommentti", start));
		while (!token.equals(new TokenKeyword(KeywordType.END_BLOCK))) {
			lexer.next();
			token = peekRequiredToken(() -> new ParseException("sulkematon kommentti", start));
		}
		lexer.next();
	}

	private boolean advanceIf(boolean condition) throws IOException, ParseException {
		if (condition)
			lexer.next();
		return condition;
	}

}
