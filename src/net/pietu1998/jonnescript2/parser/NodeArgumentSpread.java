package net.pietu1998.jonnescript2.parser;

import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.SequenceValue;
import net.pietu1998.util.DumpWriter;

public class NodeArgumentSpread extends Node implements ArgumentNode {

	private final EvaluableNode value;

	public NodeArgumentSpread(Token location, EvaluableNode value) {
		super(location);
		this.value = value;
	}

	@Override
	public void addArguments(Context context, List<JonneObject> arguments) {
		JonneObject list = value.evaluate(context);
		if (!(list instanceof SequenceValue))
			throw new ExecutionException("tyyppiä " + list.getTypeName() + " ei voi levittää", this);
		arguments.addAll(((SequenceValue) list).asList());
	}

	@Override
	public NodeExecutionResult execute(Context context) {
		throw new IllegalStateException("should not be executed");
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.print("spread");
		writer.dumpIndented(value);
	}

}
