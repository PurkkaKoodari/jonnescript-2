package net.pietu1998.jonnescript2.parser;

import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.types.JonneObject;

public interface ArgumentNode extends INode {
	
	void addArguments(Context context, List<JonneObject> arguments);

}
