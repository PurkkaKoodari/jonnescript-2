package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.util.DumpWriter;

public class NodeOperatorAssign extends EvaluableNode implements AssignableNode {

	private AssignableNode left;
	private EvaluableNode right;

	public NodeOperatorAssign(Token location, AssignableNode left, EvaluableNode right) {
		super(location);
		if (!left.isAssignable())
			throw new IllegalArgumentException("left must be assignable");
		this.left = left;
		this.right = right;
	}

	@Override
	public JonneObject evaluate(Context context) {
		JonneObject rightVal = right.evaluate(context);
		left.assign(rightVal, context);
		return rightVal;
	}

	@Override
	public boolean isAssignable() {
		return left.isAssignable() && right instanceof AssignableNode && ((AssignableNode) right).isAssignable();
	}

	@Override
	public void assign(JonneObject value, Context context) {
		if (!isAssignable())
			throw new IllegalStateException("not assignable");
		((AssignableNode) right).assign(value, context);
		left.assign(value, context);
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("assign operator");
		writer.dumpIndented(left);
		writer.dumpIndented(right);
	}
}
