package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.unicode.UnicodeString;

public interface PropertyNameNode extends INode {

	UnicodeString getPropertyName(Context context);

}
