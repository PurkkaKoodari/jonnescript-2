package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.util.DumpWriter;

public class NodeLiteralNull extends EvaluableNode {

	public NodeLiteralNull(Token location) {
		super(location);
	}

	@Override
	public JonneObject evaluate(Context context) {
		return Wrappers.wrapNull();
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("null literal");
	}

}
