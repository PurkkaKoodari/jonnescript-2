package net.pietu1998.jonnescript2.parser;

import java.util.ArrayList;
import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.util.DumpWriter;

public class NodeArray extends EvaluableNode {

	private final List<ArgumentNode> elements;

	public NodeArray(Token location, List<ArgumentNode> elements) {
		super(location);
		this.elements = elements;
	}

	@Override
	public JonneObject evaluate(Context context) {
		ArrayList<JonneObject> list = new ArrayList<>();
		elements.forEach(item -> item.addArguments(context, list));
		return Wrappers.wrap(list);
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("array");
		writer.indent();
		for (ArgumentNode element : elements)
			writer.dump(element);
		writer.dedent();
	}

}
