package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.util.DumpWriter;

public class NodeReturn extends Node {

	private final EvaluableNode value;

	public NodeReturn(Token location, EvaluableNode value) {
		super(location);
		this.value = value;
	}

	@Override
	public NodeExecutionResult execute(Context context) {
		return NodeExecutionResult.ofReturned(value.evaluate(context));
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("return");
		writer.dumpIndented(value);
	}

}
