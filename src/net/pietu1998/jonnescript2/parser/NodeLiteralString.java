package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.util.DumpWriter;

public class NodeLiteralString extends EvaluableNode {

	private final UnicodeString value;

	public NodeLiteralString(Token location, UnicodeString value) {
		super(location);
		this.value = value;
	}

	@Override
	public JonneObject evaluate(Context context) {
		return Wrappers.wrap(value);
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.print("string literal (");
		writer.print(value.toString());
		writer.println(")");
	}

}
