package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.ThrowableValue;
import net.pietu1998.util.DumpWriter;

public class NodeThrow extends Node {

	private final EvaluableNode exception;

	public NodeThrow(Token location, EvaluableNode value) {
		super(location);
		this.exception = value;
	}

	@Override
	public NodeExecutionResult execute(Context context) {
		JonneObject value = exception.evaluate(context);
		if (value instanceof ThrowableValue)
			((ThrowableValue) value).throwException(this);
		else
			throw new ExecutionException("tyyppiä " + value.getTypeName() + " ei voi heittää", this);
		throw new IllegalStateException("throwException() should always throw");
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("return");
		writer.dumpIndented(exception);
	}

}
