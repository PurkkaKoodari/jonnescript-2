package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.util.DumpWriter;

public class NodeConditionLoop extends Node {

	private final EvaluableNode condition;
	private final NodeBlock code;
	private final boolean until;

	public NodeConditionLoop(Token location, EvaluableNode condition, NodeBlock code, boolean until) {
		super(location);
		this.condition = condition;
		this.code = code;
		this.until = until;
	}

	@Override
	public NodeExecutionResult execute(Context context) {
		NodeExecutionResult result = NodeExecutionResult.empty();
		while (condition.evaluate(context).toJonneBoolean(context).value != until) {
			NodeExecutionResult newResult = code.execute(context);
			if (newResult.result.isPresent())
				result = newResult;
		}
		return result;
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.print("condition loop (");
		writer.print(until ? "until" : "while");
		writer.print(")");
		writer.dumpIndented(condition);
		writer.dumpIndented(code);
	}

}
