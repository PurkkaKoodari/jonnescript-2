package net.pietu1998.jonnescript2.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.util.DumpWriter;
import net.pietu1998.util.Dumpable;

public class FormalParameterList implements Dumpable {

	private final UnicodeString[] arguments;
	private final int varargPosition;

	public FormalParameterList(UnicodeString[] arguments, int varargPosition) {
		this.arguments = arguments;
		this.varargPosition = varargPosition;
	}

	public void resolve(Context context, JonneObject[] args) {
		if (varargPosition != -1) {
			int varargsEnd = Math.max(varargPosition, args.length - arguments.length + varargPosition + 1);
			int max = Math.max(args.length, arguments.length - 1);
			List<JonneObject> varargsData = new ArrayList<>();
			int j = 0;
			if (varargsEnd == 0)
				context.setVariable(arguments[j++], Wrappers.wrap(varargsData));
			for (int i = 0; i < max; i++) {
				if (varargPosition <= i && i < varargsEnd)
					varargsData.add(i >= args.length ? Wrappers.wrapNull() : args[i]);
				else
					context.setVariable(arguments[j++], i >= args.length ? Wrappers.wrapNull() : args[i]);
				if (i == varargsEnd - 1)
					context.setVariable(arguments[j++], Wrappers.wrap(varargsData));
			}
		} else {
			for (int i = 0; i < arguments.length; i++)
				context.setVariable(arguments[i], i >= args.length ? Wrappers.wrapNull() : args[i]);
		}
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.print(IntStream.range(0, arguments.length).mapToObj(
				i -> arguments[i] + (i == varargPosition ? " vararg" : "")).collect(Collectors.joining(", ")));
	}

}
