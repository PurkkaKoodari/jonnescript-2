package net.pietu1998.jonnescript2.parser;

import java.util.Optional;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.operator.Arity;
import net.pietu1998.jonnescript2.operator.OperatorType;
import net.pietu1998.jonnescript2.operator.UndefinedOperatorException;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.util.DumpWriter;

public class NodeOperatorUnary extends EvaluableNode {

	private final OperatorType type;
	private final EvaluableNode operand;

	public NodeOperatorUnary(Token location, OperatorType type, EvaluableNode operand) {
		super(location);
		this.type = type;
		this.operand = operand;
	}

	@Override
	public JonneObject evaluate(Context context) {
		JonneObject operandVal = operand.evaluate(context);
		try {
			return operandVal.callMethod(context, Optional.of(this), type.methodName(Arity.UNARY), Optional.empty());
		} catch (UndefinedOperatorException e) {
			throw new ExecutionException("määrittelemätön operaatio: " + type.sourceString + " "
					+ operandVal.getTypeName(), this);
		}
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.print("unary operator (");
		writer.print(type.toString());
		writer.println(")");
		writer.dumpIndented(operand);
	}

}
