package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.util.DumpWriter;

public class NodeIf extends Node {

	private final EvaluableNode condition;
	private final NodeBlock thenBlock, elseBlock;

	public NodeIf(Token location, EvaluableNode condition, NodeBlock thenBlock, NodeBlock elseBlock) {
		super(location);
		this.condition = condition;
		this.thenBlock = thenBlock;
		this.elseBlock = elseBlock;
	}

	@Override
	public NodeExecutionResult execute(Context context) {
		if (condition.evaluate(context).toJonneBoolean(context).value)
			return thenBlock.execute(context);
		else if (elseBlock != null)
			return elseBlock.execute(context);
		else
			return NodeExecutionResult.empty();
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("if");
		writer.dumpIndented(condition);
		writer.dumpIndented(thenBlock);
		if (elseBlock != null)
			writer.dumpIndented(elseBlock);
	}

}
