package net.pietu1998.jonnescript2.parser;

import java.math.BigInteger;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.util.DumpWriter;

public class NodeMoped extends Node {

	private static final BigInteger THOUSAND_FACTORIAL;

	static {
		BigInteger factorial = BigInteger.ONE;
		for (int i = 2; i <= 1000; i++)
			factorial = factorial.multiply(BigInteger.valueOf(i));
		THOUSAND_FACTORIAL = factorial;
	}

	public NodeMoped(Token location) {
		super(location);
	}

	@Override
	public NodeExecutionResult execute(Context context) {
		System.out.println(THOUSAND_FACTORIAL.toString());
		return NodeExecutionResult.of(Wrappers.wrap(THOUSAND_FACTORIAL));
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("moped");
	}

}
