package net.pietu1998.jonnescript2.parser;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.exception.ParseException;
import net.pietu1998.jonnescript2.lexer.Lexer;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.lexer.Tokenizer;
import net.pietu1998.jonnescript2.types.JonneFunctionBase;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.JonneString;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.util.DumpWriter;

public class NodeOperatorCall extends EvaluableNode {

	private final EvaluableNode left;
	private final List<ArgumentNode> arguments;

	public NodeOperatorCall(Token location, EvaluableNode left, List<ArgumentNode> arguments) {
		super(location);
		this.left = left;
		this.arguments = new ArrayList<>(arguments);
	}

	@Override
	public JonneObject evaluate(Context context) {
		JonneObject leftVal = left.evaluate(context);
		if (leftVal instanceof JonneFunctionBase) {
			List<JonneObject> argumentVals = new ArrayList<>();
			arguments.forEach(arg -> arg.addArguments(context, argumentVals));
			JonneFunctionBase function = (JonneFunctionBase) leftVal;
			return function.run(context, this, argumentVals.toArray(new JonneObject[0]));
		}
		if (leftVal instanceof JonneString) {
			if (!arguments.isEmpty())
				throw new ExecutionException("merkkijonon suorittamiselle ei voi antaa argumentteja", this);
			try {
				Reader reader = new StringReader(((JonneString) leftVal).value.toString());
				Tokenizer tokenizer = new Tokenizer(reader, "<merkkijono>");
				Lexer lexer = new Lexer(tokenizer);
				Parser parser = new Parser(lexer);
				Node parsed = parser.parseBlock();
				return parsed.execute(context).result.orElseGet(Wrappers::wrapNull);
			} catch (ExecutionException e) {
				throw e.inside(null, this);
			} catch (ParseException e) {
				throw new ExecutionException(e.toString(), this);
			} catch (IOException e) {
				throw new ExecutionException("virhe suoritettaessa koodia", this);
			}
		}
		throw new ExecutionException("tyyppiä " + leftVal.getTypeName() + " ei voida kutsua", this);
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("call operator");
		writer.dumpIndented(left);
		writer.indent();
		for (ArgumentNode node : arguments) {
			writer.dump(node);
		}
		writer.dedent();
	}

}
