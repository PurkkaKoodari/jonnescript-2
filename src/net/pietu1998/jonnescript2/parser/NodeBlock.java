package net.pietu1998.jonnescript2.parser;

import java.util.ArrayList;
import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.util.DumpWriter;

/**
 * A node that is a code block, i.e. a collection of statements.
 */
public class NodeBlock extends Node {

	private final List<Node> statements;

	public NodeBlock(List<Node> statements) {
		this.statements = new ArrayList<>(statements);
	}

	@Override
	public NodeExecutionResult execute(Context context) {
		NodeExecutionResult result = NodeExecutionResult.empty();
		for (Node child : statements) {
			NodeExecutionResult newResult = child.execute(context);
			if (newResult.result.isPresent()) {
				result = newResult;
				if (result.returned)
					break;
			}
		}
		return result;
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("block");
		for (Node node : statements) {
			writer.dumpIndented(node);
		}
	}
}
