package net.pietu1998.jonnescript2.parser;

import java.util.Optional;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.operator.Arity;
import net.pietu1998.jonnescript2.operator.OperatorType;
import net.pietu1998.jonnescript2.operator.OverrideMode;
import net.pietu1998.jonnescript2.operator.UndefinedOperatorException;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.util.DumpWriter;

public class NodeOperatorBinary extends EvaluableNode implements AssignableNode {

	private final OperatorType type;
	private final EvaluableNode left, right;

	public NodeOperatorBinary(Token location, OperatorType type, EvaluableNode left, EvaluableNode right) {
		super(location);
		this.type = type;
		this.left = left;
		this.right = right;
	}

	@Override
	public JonneObject evaluate(Context context) {
		JonneObject leftVal = left.evaluate(context);
		JonneObject rightVal = right.evaluate(context);
		try {
			return leftVal.callMethod(context, Optional.of(this), type.methodName(Arity.BINARY), Optional.empty(),
					rightVal);
		} catch (UndefinedOperatorException e) {
			throw new ExecutionException("määrittelemätön operaatio: " + leftVal.getTypeName() + " "
					+ type.sourceString + " " + rightVal.getTypeName(), this);
		}
	}

	@Override
	public boolean isAssignable() {
		return type.overrideMode == OverrideMode.READ_WRITE;
	}

	@Override
	public void assign(JonneObject value, Context context) {
		JonneObject leftVal = left.evaluate(context);
		JonneObject rightVal = right.evaluate(context);
		try {
			leftVal.callMethod(context, Optional.of(this), type.methodName(Arity.BINARY, true), Optional.empty(),
					rightVal, value);
		} catch (UndefinedOperatorException e) {
			throw new ExecutionException("määrittelemätön operaatio: " + leftVal.getTypeName() + " "
					+ type.sourceString + " " + rightVal.getTypeName(), this);
		}
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.print("binary operator (");
		writer.print(type.toString());
		writer.println(")");
		writer.dumpIndented(left);
		writer.dumpIndented(right);
	}
}
