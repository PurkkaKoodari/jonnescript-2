package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.types.JonneObject;

/**
 * A node representing a location a value can be assigned to.
 */
public interface AssignableNode extends INode {

	/**
	 * Assigns a value to the location represented by this node.
	 * 
	 * @param value
	 *            the value to assign.
	 * @param context
	 *            the context in which to perform the assignment.
	 */
	void assign(JonneObject value, Context context);

	/**
	 * Checks whether this node can actually be assigned to.
	 * 
	 * @return {@code true} if and only if this node can be assigned to,
	 *         otherwise {@code false}.
	 */
	boolean isAssignable();

}
