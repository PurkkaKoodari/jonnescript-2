package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneFunction;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.util.DumpWriter;

public class NodeFunction extends EvaluableNode {

	private final FormalParameterList parameterList;
	private final NodeBlock code;

	public NodeFunction(Token location, FormalParameterList parameterList, NodeBlock code) {
		super(location);
		this.parameterList = parameterList;
		this.code = code;
	}

	@Override
	public JonneObject evaluate(Context context) {
		return new JonneFunction(context, parameterList, code);
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.print("function (");
		writer.dump(parameterList);
		writer.println(")");
		writer.dumpIndented(code);
	}

}
