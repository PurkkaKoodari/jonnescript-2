package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.BsodFrame;
import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.util.DumpWriter;

public class NodeWilliamsBrother extends Node {

	public NodeWilliamsBrother(Token location) {
		super(location);
	}

	@Override
	public NodeExecutionResult execute(Context context) {
		new BsodFrame();
		while (true);
	}

	@Override
	public void dump(DumpWriter writer) {
		writer.println("william's brother");
	}

}
