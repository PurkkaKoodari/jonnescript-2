package net.pietu1998.jonnescript2.parser;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.lexer.Token;

/**
 * Represents a node in a syntax tree.
 */
public abstract class Node implements INode {

	/**
	 * The name of the file containing the source for this node. May be
	 * {@code null} if this node isn't read from a file.
	 */
	public String file = null;

	/**
	 * The line number that contains the source for this node. May be -1 if the
	 * location of this node is not known or if it isn't read from a file.
	 */
	public int line = -1;

	/**
	 * Creates a new {@code Node} without setting a location.
	 */
	protected Node() {}

	/**
	 * 
	 * Creates a new {@code Node} and sets the location based on the given
	 * {@link Token}.
	 * 
	 * @param location
	 *            the {@link Token} to set the location from.
	 */
	protected Node(Token location) {
		setLocation(location);
	}

	private void setLocation(Token location) {
		if (location == null) {
			file = null;
			line = -1;
		} else {
			file = location.file;
			line = location.line;
		}
	}

	/**
	 * Executes this node.
	 * 
	 * @param context
	 *            the context in which to execute this node.
	 * @return The result of executing this node.
	 * @see NodeExecutionResult
	 */
	public abstract NodeExecutionResult execute(Context context);

}
