package net.pietu1998.jonnescript2.parser;

import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.NodeExecutionResult;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.types.JonneString;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.unicode.UnicodeString;

/**
 * A node that can be evaluated in order to produce a value.
 */
public abstract class EvaluableNode extends Node implements PropertyNameNode, ArgumentNode {

	/**
	 * Creates a new {@code EvaluableNode} without setting a location.
	 */
	protected EvaluableNode() {
	}

	/**
	 * Creates a new {@code EvaluableNode} and sets the location based on the given {@link Token}.
	 * 
	 * @param location
	 *            the {@link Token} to set the location from.
	 */
	protected EvaluableNode(Token location) {
		super(location);
	}

	@Override
	public NodeExecutionResult execute(Context context) {
		return NodeExecutionResult.of(evaluate(context));
	}

	@Override
	public UnicodeString getPropertyName(Context context) {
		JonneObject value = evaluate(context);
		if (value instanceof JonneString)
			return ((JonneString) value).value;
		else
			throw new ExecutionException(value.getTypeName() + " ei voi olla ominaisuuden nimi", this);
	}

	/**
	 * Evaluates this node in order to produce a value.
	 * 
	 * @param context
	 *            the context in which to perform the evaluation.
	 * @return The value of this node.
	 */
	public abstract JonneObject evaluate(Context context);
	
	@Override
	public void addArguments(Context context, List<JonneObject> arguments) {
		arguments.add(evaluate(context));
	}

}
