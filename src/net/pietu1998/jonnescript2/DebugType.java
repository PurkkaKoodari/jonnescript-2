package net.pietu1998.jonnescript2;

import net.pietu1998.jonnescript2.lexer.Lexer;
import net.pietu1998.jonnescript2.lexer.StringToken;
import net.pietu1998.jonnescript2.lexer.Token;
import net.pietu1998.jonnescript2.lexer.Tokenizer;
import net.pietu1998.jonnescript2.parser.Node;
import net.pietu1998.jonnescript2.parser.Parser;

/**
 * Enumeration of the possible debugging types.
 */
public enum DebugType {
	/**
	 * No debugging.
	 */
	NONE,
	/**
	 * Dump the stream of {@link StringToken}s output by the {@link Tokenizer}.
	 */
	TOKENIZER,
	/**
	 * Dump the stream of {@link Token}s output by the {@link Lexer}.
	 */
	LEXER,
	/**
	 * Dump the {@link Node} tree output by the {@link Parser}.
	 */
	PARSER,
	/**
	 * Dump the stack trace on any runtime or parser errors.
	 */
	ERRORS
}
