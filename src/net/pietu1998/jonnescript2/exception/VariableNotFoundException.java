package net.pietu1998.jonnescript2.exception;

import net.pietu1998.unicode.UnicodeString;

public class VariableNotFoundException extends Exception {

	private final UnicodeString variableName;

	public VariableNotFoundException(UnicodeString property) {
		this.variableName = property;
	}

	public UnicodeString getVariableName() {
		return variableName;
	}

}
