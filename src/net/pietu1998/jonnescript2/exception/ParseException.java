package net.pietu1998.jonnescript2.exception;

import net.pietu1998.jonnescript2.lexer.StringToken;
import net.pietu1998.jonnescript2.lexer.Token;

public class ParseException extends Exception {

	private String file = null;
	private int line = -1;

	public ParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParseException(String message) {
		super(message);
	}

	public ParseException(Throwable cause) {
		super(cause);
	}

	public ParseException(String message, Throwable cause, String file) {
		super(message, cause);
		setLocation(file);
	}

	public ParseException(String message, String file) {
		super(message);
		setLocation(file);
	}

	public ParseException(Throwable cause, String file) {
		super(cause);
		setLocation(file);
	}

	public ParseException(String message, Throwable cause, StringToken location) {
		super(message, cause);
		setLocation(location);
	}

	public ParseException(String message, StringToken location) {
		super(message);
		setLocation(location);
	}

	public ParseException(Throwable cause, StringToken location) {
		super(cause);
		setLocation(location);
	}

	public ParseException(String message, Throwable cause, Token location) {
		super(message, cause);
		setLocation(location);
	}

	public ParseException(String message, Token location) {
		super(message);
		setLocation(location);
	}

	public ParseException(Throwable cause, Token location) {
		super(cause);
		setLocation(location);
	}

	public void setLocation(StringToken location) {
		if (location == null) {
			file = null;
			line = -1;
		} else {
			file = location.file;
			line = location.line;
		}
	}

	public void setLocation(Token location) {
		if (location == null) {
			file = null;
			line = -1;
		} else {
			file = location.file;
			line = location.line;
		}
	}

	public void setLocation(String file) {
		this.file = file;
		this.line = -1;
	}

	@Override
	public String toString() {
		if (getMessage() == null) {
			if (file == null)
				return "säsenninvirhe";
			else if (line == -1)
				return "säsenninvirhe tiedostossa " + file;
			else
				return "säsenninvirhe tiedostossa " + file + " rivillä " + line;
		} else {
			if (file == null)
				return "syntaksivirhe: " + getMessage();
			else if (line == -1)
				return "syntaksivirhe: " + getMessage() + " tiedostossa " + file;
			else
				return "syntaksivirhe: " + getMessage() + " tiedostossa " + file + " rivillä " + line;
		}
	}
}
