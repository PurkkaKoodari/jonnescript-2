package net.pietu1998.jonnescript2.exception;

public class StackTraceLevel {

	private final String function, file;
	private final int line;

	public StackTraceLevel(String function, String file, int line) {
		if (line != -1 && file == null)
			throw new IllegalArgumentException("line was specified but not file");
		this.function = function;
		this.file = file;
		this.line = line;
	}

	public String getFunction() {
		return function;
	}

	public String getFile() {
		return file;
	}

	public int getLine() {
		return line;
	}

	public boolean hasLocation() {
		return function != null || file != null;
	}

	@Override
	public String toString() {
		if (function == null)
			if (file == null)
				return "tuntemattomassa kohteessa";
			else if (line == -1)
				return "tiedostossa " + file;
			else
				return "tiedostossa " + file + " rivillä " + line;
		else if (file == null)
			return "funktiossa " + function;
		else if (line == -1)
			return "funktiossa " + function + " tiedostossa " + file;
		else
			return "funktiossa " + function + " tiedostossa " + file + " rivillä " + line;
	}
}
