package net.pietu1998.jonnescript2.exception;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.pietu1998.jonnescript2.parser.Node;

/**
 * Represents a runtime error caused by JonneScript 2.
 */
public class ExecutionException extends RuntimeException {

	private List<StackTraceLevel> stack;

	/**
	 * Creates an {@code ExecutionException} with the given message and no
	 * location.
	 * 
	 * @param message
	 *            the message to set.
	 */
	public ExecutionException(String message) {
		super(message);
		this.stack = new ArrayList<>();
	}

	/**
	 * Creates an {@code ExecutionException} with a stack trace containing the
	 * given node and function name.
	 * 
	 * @param message
	 *            the message to set.
	 * @param node
	 *            the node that caused this exception.
	 */
	public ExecutionException(String message, Node node) {
		this(message);
		if (node == null)
			throw new IllegalArgumentException("node must not be null");
		stack.add(new StackTraceLevel(null, node.file, node.line));
	}

	private ExecutionException(List<StackTraceLevel> stackTrace, String message) {
		super(message);
		this.stack = stackTrace;
	}

	/**
	 * Creates a new {@code ExecutionException} that indicates that the
	 * exception occurred inside the given function. This is done by changing
	 * the function name of the last stack trace level and creating a new one.
	 * 
	 * @param function
	 *            the function name of the stack trace level, or {@code null} if
	 *            none.
	 * @param node
	 *            the node that caused this exception, or {@code null} if none.
	 * @return A new {@code ExceutionException}.
	 */
	public ExecutionException inside(String function, Node node) {
		List<StackTraceLevel> newStack = new ArrayList<>(stack);
		if (newStack.isEmpty()) {
			newStack.add(new StackTraceLevel(function, null, -1));
		} else {
			int lastIndex = newStack.size() - 1;
			StackTraceLevel last = newStack.get(lastIndex);
			newStack.set(lastIndex, new StackTraceLevel(function, last.getFile(), last.getLine()));
		}
		if (node == null)
			newStack.add(new StackTraceLevel(null, null, -1));
		else
			newStack.add(new StackTraceLevel(null, node.file, node.line));
		return new ExecutionException(newStack, getMessage());
	}

	/**
	 * Gets the stack trace of this exception.
	 * 
	 * @return The stack trace of this exception.
	 */
	public List<StackTraceLevel> getStack() {
		return Collections.unmodifiableList(stack);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("virhe: ").append(getMessage());
		for (StackTraceLevel level : stack)
			if (level.hasLocation())
				builder.append('\n').append(level.toString());
		return builder.toString();
	}

}
