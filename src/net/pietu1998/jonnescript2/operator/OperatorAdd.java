package net.pietu1998.jonnescript2.operator;

import java.util.ArrayList;
import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.types.JonneArray;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.jonnescript2.types.JonneNumber;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.JonneString;
import net.pietu1998.jonnescript2.types.NumericValue;
import net.pietu1998.jonnescript2.types.Wrappers;

public class OperatorAdd implements BinaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal) {
		if (leftVal instanceof JonneNull && rightVal instanceof JonneNull) {
			return Wrappers.wrapNull();
		}
		if (leftVal instanceof NumericValue && rightVal instanceof NumericValue) {
			return addNumeric(leftVal.toJonneNumber(context), rightVal.toJonneNumber(context));
		}
		if (leftVal instanceof JonneArray && rightVal instanceof JonneArray) {
			return concatArrays((JonneArray) leftVal, (JonneArray) rightVal);
		}
		if (leftVal instanceof JonneArray) {
			return appendArray((JonneArray) leftVal, rightVal);
		}
		if (rightVal instanceof JonneArray) {
			return prependArray((JonneArray) rightVal, leftVal);
		}
		return concatStrings(leftVal.toJonneString(context), rightVal.toJonneString(context));
	}

	private static JonneObject addNumeric(JonneNumber leftVal, JonneNumber rightVal) {
		return Wrappers.wrap(leftVal.value.add(rightVal.value));
	}

	private static JonneObject concatStrings(JonneString leftVal, JonneString rightVal) {
		return Wrappers.wrap(leftVal.value.concat(rightVal.value));
	}

	private static JonneObject concatArrays(JonneArray leftVal, JonneArray rightVal) {
		List<JonneObject> data = new ArrayList<>(leftVal.data);
		data.addAll(rightVal.data);
		return Wrappers.wrap(data);
	}

	private static JonneObject appendArray(JonneArray array, JonneObject value) {
		List<JonneObject> data = new ArrayList<>(array.data);
		data.add(value);
		return Wrappers.wrap(data);
	}

	private static JonneObject prependArray(JonneArray array, JonneObject value) {
		List<JonneObject> data = new ArrayList<>();
		data.add(value);
		data.addAll(array.data);
		return Wrappers.wrap(data);
	}

}
