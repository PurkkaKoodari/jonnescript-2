package net.pietu1998.jonnescript2.operator;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.NumericValue;
import net.pietu1998.jonnescript2.types.SequenceValue;

public class OperatorIndex implements BinaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal) {
		if (!(leftVal instanceof SequenceValue))
			throw new ExecutionException("tyyppiä " + leftVal.getTypeName() + " ei voi indeksoida");
		if (!(rightVal instanceof NumericValue))
			throw new ExecutionException("tyyppi " + rightVal.getTypeName() + " ei voi olla indeksi");
		try {
			return ((SequenceValue) leftVal).get(rightVal.toJonneNumber(context).value.intValueExact());
		} catch (ArithmeticException e) {
			throw new ExecutionException("indeksin täytyy olla kokonaisluku");
		} catch (IndexOutOfBoundsException e) {
			throw new ExecutionException("indeksi on alueen ulkopuolella");
		}
	}

}
