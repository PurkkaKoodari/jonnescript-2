package net.pietu1998.jonnescript2.operator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.types.JonneArray;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.jonnescript2.types.JonneNumber;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.ParseableNumericValue;
import net.pietu1998.jonnescript2.types.Wrappers;

public class OperatorNegate implements UnaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject operandVal) {
		if (operandVal instanceof JonneNull) {
			return Wrappers.wrapNull();
		}
		if (operandVal instanceof ParseableNumericValue) {
			return negateNumeric(operandVal.toJonneNumber(context));
		}
		if (operandVal instanceof JonneArray) {
			return reverseArray((JonneArray) operandVal);
		}
		throw new UndefinedOperatorException();
	}

	private static JonneObject negateNumeric(JonneNumber operandVal) {
		return Wrappers.wrap(operandVal.value.negate());
	}

	private static JonneObject reverseArray(JonneArray operandVal) {
		List<JonneObject> data = new ArrayList<>(operandVal.data);
		Collections.reverse(data);
		return Wrappers.wrap(data);
	}
}
