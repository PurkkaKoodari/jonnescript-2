package net.pietu1998.jonnescript2.operator;

import java.util.List;
import java.util.function.IntPredicate;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.JonneString;
import net.pietu1998.jonnescript2.types.NumericValue;
import net.pietu1998.jonnescript2.types.SequenceValue;
import net.pietu1998.jonnescript2.types.Wrappers;

public class OperatorComparison implements BinaryOperator {

	private final IntPredicate predicate;

	public OperatorComparison(IntPredicate predicate) {
		this.predicate = predicate;
	}

	@Override
	public JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal) {
		return Wrappers.wrap(predicate.test(compare(context, leftVal, rightVal)));
	}

	private static int compare(Context context, JonneObject leftVal, JonneObject rightVal) {
		if (leftVal.equals(rightVal)) {
			return 0;
		}
		if (leftVal instanceof NumericValue && rightVal instanceof NumericValue) {
			return compareNumeric(context, leftVal, rightVal);
		}
		if (leftVal instanceof JonneString && rightVal instanceof JonneString) {
			return compareString((JonneString) leftVal, (JonneString) rightVal);
		}
		if (leftVal instanceof SequenceValue && rightVal instanceof SequenceValue) {
			return compareSequence(context, (SequenceValue) leftVal, (SequenceValue) rightVal);
		}
		throw new UndefinedOperatorException();
	}

	private static int compareNumeric(Context context, JonneObject leftVal, JonneObject rightVal) {
		return leftVal.toJonneNumber(context).value.compareTo(rightVal.toJonneNumber(context).value);
	}

	private static int compareString(JonneString leftVal, JonneString rightVal) {
		return leftVal.value.compareTo(rightVal.value);
	}

	private static int compareSequence(Context context, SequenceValue leftVal, SequenceValue rightVal) {
		List<JonneObject> leftList = leftVal.asList();
		List<JonneObject> rightList = rightVal.asList();
		int length = Math.min(leftList.size(), rightList.size());
		for (int i = 0; i < length; i++) {
			int comparison = compare(context, leftList.get(i), rightList.get(i));
			if (comparison != 0)
				return comparison;
		}
		return leftList.size() < rightList.size() ? -1 : leftList.size() > rightList.size() ? 1 : 0;
	}

}
