package net.pietu1998.jonnescript2.operator;

import net.pietu1998.unicode.UnicodeString;

public enum Arity {
	UNARY("unaari"),
	BINARY("binaari");

	public final UnicodeString friendlyName;

	Arity(String friendlyName) {
		this.friendlyName = new UnicodeString(friendlyName);
	}
}
