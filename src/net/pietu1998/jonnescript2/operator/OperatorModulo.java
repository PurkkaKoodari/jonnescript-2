package net.pietu1998.jonnescript2.operator;

import java.util.ArrayList;
import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.types.JonneArray;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.jonnescript2.types.JonneNumber;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.JonneString;
import net.pietu1998.jonnescript2.types.NumericValue;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.unicode.UnicodeStringBuilder;

public class OperatorModulo implements BinaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal) {
		if (leftVal instanceof JonneNull && rightVal instanceof JonneNull) {
			return Wrappers.wrapNull();
		}
		if (leftVal instanceof NumericValue && rightVal instanceof NumericValue) {
			return remainderNumeric(leftVal.toJonneNumber(context), rightVal.toJonneNumber(context));
		}
		if (leftVal instanceof JonneString && rightVal instanceof NumericValue) {
			return moduloString((JonneString) leftVal, rightVal.toJonneNumber(context));
		}
		if (leftVal instanceof JonneArray && rightVal instanceof NumericValue) {
			return moduloArray((JonneArray) leftVal, rightVal.toJonneNumber(context));
		}
		throw new UndefinedOperatorException();
	}

	private static JonneObject remainderNumeric(JonneNumber leftVal, JonneNumber rightVal) {
		try {
			return Wrappers.wrap(leftVal.value.remainder(rightVal.value));
		} catch (ArithmeticException e) {
			throw new ExecutionException("nollalla ei voi jakaa");
		}
	}

	private static JonneObject moduloString(JonneString leftVal, JonneNumber rightVal) {
		try {
			int step = rightVal.value.intValueExact();
			if (step == 0)
				throw new ExecutionException("modulo-askel ei voi olla nolla");
			UnicodeStringBuilder builder = new UnicodeStringBuilder();
			UnicodeString string = leftVal.value;
			for (int i = step > 0 ? 0 : string.length() - 1; i >= 0 && i < string.length(); i += step)
				builder.append(string.codePointAt(i));
			return Wrappers.wrap(builder.toUnicodeString());
		} catch (ArithmeticException e) {
			throw new ExecutionException("modulo-askeleen tulee olla kokonaisluku");
		}
	}

	private static JonneObject moduloArray(JonneArray leftVal, JonneNumber rightVal) {
		try {
			int step = rightVal.value.intValueExact();
			if (step == 0)
				throw new ExecutionException("modulo-askel ei voi olla nolla");
			List<JonneObject> array = leftVal.data;
			List<JonneObject> result = new ArrayList<>();
			for (int i = step > 0 ? 0 : array.size() - 1; i >= 0 && i < array.size(); i += step)
				result.add(array.get(i));
			return Wrappers.wrap(result);
		} catch (ArithmeticException e) {
			throw new ExecutionException("modulo-askeleen tulee olla kokonaisluku");
		}
	}
}
