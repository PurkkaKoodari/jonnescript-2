package net.pietu1998.jonnescript2.operator;

import java.math.BigInteger;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.types.JonneBoolean;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.jonnescript2.types.JonneNumber;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.NumericValue;
import net.pietu1998.jonnescript2.types.Wrappers;

public class OperatorNot implements UnaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject operandVal) {
		if (operandVal instanceof JonneNull) {
			return Wrappers.wrapNull();
		}
		if (operandVal instanceof JonneBoolean) {
			return notBoolean((JonneBoolean) operandVal);
		}
		if (operandVal instanceof NumericValue) {
			return invertNumeric(operandVal.toJonneNumber(context));
		}
		throw new UndefinedOperatorException();
	}

	private static JonneObject notBoolean(JonneBoolean operandVal) {
		return Wrappers.wrap(!operandVal.value);
	}

	private static JonneObject invertNumeric(JonneNumber operandVal) {
		try {
			BigInteger operandInt = operandVal.value.toBigIntegerExact();
			return Wrappers.wrap(operandInt.not());
		} catch (ArithmeticException e) {
			throw new ExecutionException(OperatorType.NOT.sourceString + " ei ole määritelty ei-kokonaisluvuille");
		}
	}

}
