package net.pietu1998.jonnescript2.operator;

import java.util.regex.Pattern;
import java.util.stream.Collectors;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.jonnescript2.types.JonneNumber;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.JonneString;
import net.pietu1998.jonnescript2.types.NumericValue;
import net.pietu1998.jonnescript2.types.Wrappers;

public class OperatorDivide implements BinaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal) {
		if (leftVal instanceof JonneNull && rightVal instanceof JonneNull) {
			return Wrappers.wrapNull();
		}
		if (leftVal instanceof NumericValue && rightVal instanceof NumericValue) {
			return divideNumeric(leftVal.toJonneNumber(context), rightVal.toJonneNumber(context));
		}
		if (leftVal instanceof JonneString && rightVal instanceof JonneString) {
			return splitString((JonneString) leftVal, (JonneString) rightVal);
		}
		throw new UndefinedOperatorException();
	}

	private static JonneObject divideNumeric(JonneNumber leftVal, JonneNumber rightVal) {
		try {
			return Wrappers.wrap(leftVal.value.divide(rightVal.value));
		} catch (ArithmeticException e) {
			throw new ExecutionException("nollalla ei voi jakaa");
		}
	}

	private static JonneObject splitString(JonneString leftVal, JonneString rightVal) {
		Pattern pattern = Pattern.compile(rightVal.value.toString(), Pattern.LITERAL);
		return Wrappers.wrap(pattern.splitAsStream(leftVal.value.toString()).map(Wrappers::wrap).collect(
				Collectors.toList()));
	}
}
