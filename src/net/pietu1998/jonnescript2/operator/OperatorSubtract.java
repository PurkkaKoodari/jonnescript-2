package net.pietu1998.jonnescript2.operator;

import java.util.ArrayList;
import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.types.JonneArray;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.jonnescript2.types.JonneNumber;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.ParseableNumericValue;
import net.pietu1998.jonnescript2.types.Wrappers;

public class OperatorSubtract implements BinaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal) {
		if (leftVal instanceof JonneNull && rightVal instanceof JonneNull) {
			return Wrappers.wrapNull();
		}
		if (leftVal instanceof ParseableNumericValue && rightVal instanceof ParseableNumericValue) {
			return subtractNumeric(leftVal.toJonneNumber(context), rightVal.toJonneNumber(context));
		}
		if (leftVal instanceof JonneArray && rightVal instanceof JonneArray) {
			return setDifference((JonneArray) leftVal, (JonneArray) rightVal);
		}
		if (leftVal instanceof JonneArray) {
			return removeFromSet((JonneArray) leftVal, rightVal);
		}
		if (rightVal instanceof JonneArray) {
			return removeFromSetReverse(leftVal, (JonneArray) rightVal);
		}
		throw new UndefinedOperatorException();
	}

	private static JonneObject subtractNumeric(JonneNumber leftVal, JonneNumber rightVal) {
		return Wrappers.wrap(leftVal.value.subtract(rightVal.value));
	}

	private static JonneObject setDifference(JonneArray leftVal, JonneArray rightVal) {
		List<JonneObject> data = new ArrayList<>(leftVal.data);
		data.removeAll(rightVal.data);
		return Wrappers.wrap(data);
	}

	private static JonneObject removeFromSet(JonneArray array, JonneObject value) {
		List<JonneObject> data = new ArrayList<>(array.data);
		data.remove(value);
		return Wrappers.wrap(data);
	}

	private static JonneObject removeFromSetReverse(JonneObject value, JonneArray array) {
		List<JonneObject> data = new ArrayList<>();
		data.add(value);
		data.removeAll(array.data);
		return Wrappers.wrap(data);
	}

}
