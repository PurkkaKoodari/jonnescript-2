package net.pietu1998.jonnescript2.operator;

public enum OverrideMode {
	READ,
	READ_WRITE,
	NOT_OVERRIDABLE
}
