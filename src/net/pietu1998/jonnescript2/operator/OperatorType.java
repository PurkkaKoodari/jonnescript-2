package net.pietu1998.jonnescript2.operator;

import java.util.Arrays;
import java.util.EnumSet;

import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.unicode.UnicodeStringBuilder;

public enum OperatorType {
	PLUS("plus", 40, new OperatorAdd()),
	MINUS("miinus", 40, new OperatorNegate(), new OperatorSubtract()),
	MULTIPLY("kertaa", 50, new OperatorMultiply()),
	DIVIDE("jaettuna", 50, new OperatorDivide()),
	MODULO("modulo", 50, new OperatorModulo()),
	NOT("ei", new OperatorNot()),
	OR("tai", 10, new OperatorOr()),
	AND("ja", 20, new OperatorAnd()),
	XOR("poissulkeva tai", 10, new OperatorXor()),
	GREATER_THAN("on suurempi kuin", 30, new OperatorComparison(result -> result > 0)),
	LESS_THAN("on pienempi kuin", 30, new OperatorComparison(result -> result < 0)),
	GREATER_THAN_OR_EQUAL("on vähintään", 30, new OperatorComparison(result -> result >= 0)),
	LESS_THAN_OR_EQUAL("on enintään", 30, new OperatorComparison(result -> result <= 0)),
	EQUAL("on yhtä kuin", 30, new OperatorComparison(result -> result == 0)),
	IDENTITY("on sama kuin", 30, new OperatorIdentity()),
	LENGTH("pituus", new OperatorLength()),
	INDEX("kohta", 70, new OperatorIndex(), new OperatorIndexWrite()),
	PROPERTY("ominaisuus", 70, new JonneObject.OperatorProperty(), new JonneObject.OperatorPropertyWrite()),
	CALL("aja", 60),
	ASSIGN("aseta", 0, OverrideMode.NOT_OVERRIDABLE);

	public final UnicodeString[] sourceParts;
	public final UnicodeString sourceString;
	public final int binaryPrecedence;
	public final EnumSet<Arity> arity;
	public final OverrideMode overrideMode;

	public final UnaryOperator unaryImplementation;
	public final BinaryOperator binaryImplementation;
	public final BinaryWriteOperator binaryWriteImplementation;

	private final UnicodeString unaryRead, binaryRead, unaryWrite, binaryWrite;

	public final static String WRITE_NAME = "aseta";

	OperatorType(String source, int precedence) {
		this(source, precedence, EnumSet.of(Arity.BINARY), OverrideMode.READ, null, null, null);
	}

	OperatorType(String source, int precedence, OverrideMode overrideMode) {
		this(source, precedence, EnumSet.of(Arity.BINARY), overrideMode, null, null, null);
	}

	OperatorType(String source, UnaryOperator unaryImplementation) {
		this(source, Integer.MIN_VALUE, EnumSet.of(Arity.UNARY), OverrideMode.READ, unaryImplementation, null, null);
	}

	OperatorType(String source, int precedence, BinaryOperator binaryImplementation) {
		this(source, precedence, EnumSet.of(Arity.BINARY), OverrideMode.READ, null, binaryImplementation, null);
	}

	OperatorType(String source, int binaryPrecedence, UnaryOperator unaryImplementation,
				 BinaryOperator binaryImplementation) {
		this(source, binaryPrecedence, EnumSet.of(Arity.UNARY, Arity.BINARY), OverrideMode.READ, unaryImplementation,
				binaryImplementation, null);
	}

	OperatorType(String source, int binaryPrecedence, BinaryOperator binaryImplementation,
				 BinaryWriteOperator binaryWriteImplementation) {
		this(source, binaryPrecedence, EnumSet.of(Arity.UNARY, Arity.BINARY), OverrideMode.READ_WRITE, null,
				binaryImplementation, binaryWriteImplementation);
	}

	OperatorType(String source, int binaryPrecedence, EnumSet<Arity> arity, OverrideMode overrideMode,
				 UnaryOperator unaryImplementation, BinaryOperator binaryImplementation,
				 BinaryWriteOperator binaryWriteImplementation) {
		this.sourceParts = Arrays.stream(source.split(" ")).map(UnicodeString::new).toArray(UnicodeString[]::new);
		this.sourceString = new UnicodeString(source);
		this.binaryPrecedence = binaryPrecedence;
		this.arity = arity;
		this.overrideMode = overrideMode;
		this.unaryImplementation = unaryImplementation;
		this.binaryImplementation = binaryImplementation;
		this.binaryWriteImplementation = binaryWriteImplementation;
		this.unaryRead = generateMethodName(Arity.UNARY, false);
		this.binaryRead = generateMethodName(Arity.BINARY, false);
		this.unaryWrite = generateMethodName(Arity.UNARY, true);
		this.binaryWrite = generateMethodName(Arity.BINARY, true);
	}

	private UnicodeString generateMethodName(Arity arity, boolean write) {
		UnicodeStringBuilder builder = new UnicodeStringBuilder();
		builder.append(arity.friendlyName).append(' ');
		if (write)
			builder.append("aseta ");
		return builder.append(sourceString).toUnicodeString();
	}

	public UnicodeString methodName() {
		return methodName(false);
	}

	public UnicodeString methodName(boolean write) {
		if (arity.contains(Arity.BINARY))
			return methodName(Arity.BINARY, write);
		else
			return methodName(Arity.UNARY, write);
	}

	public UnicodeString methodName(Arity arity) {
		return methodName(arity, false);
	}

	public UnicodeString methodName(Arity arity, boolean write) {
		if (arity == Arity.UNARY)
			return write ? unaryWrite : unaryRead;
		else
			return write ? binaryWrite : binaryRead;
	}

}