package net.pietu1998.jonnescript2.operator;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.types.AssignableSequenceValue;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.NumericValue;

public class OperatorIndexWrite implements BinaryWriteOperator {

	@Override
	public void assign(Context context, JonneObject leftVal, JonneObject rightVal, JonneObject assignVal) {
		if (!(leftVal instanceof AssignableSequenceValue))
			throw new ExecutionException("tyyppiin " + leftVal.getTypeName() + " ei voi asettaa");
		if (!(rightVal instanceof NumericValue))
			throw new ExecutionException("tyyppi " + rightVal.getTypeName() + " ei voi olla indeksi");
		try {
			((AssignableSequenceValue) leftVal).set(rightVal.toJonneNumber(context).value.intValueExact(), assignVal);
		} catch (ArithmeticException e) {
			throw new ExecutionException("indeksin täytyy olla kokonaisluku");
		}
	}

}
