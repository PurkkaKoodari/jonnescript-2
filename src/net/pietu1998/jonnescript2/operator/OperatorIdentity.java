package net.pietu1998.jonnescript2.operator;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.Wrappers;

public class OperatorIdentity implements BinaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal) {
		return Wrappers.wrap(leftVal == rightVal);
	}

}
