package net.pietu1998.jonnescript2.operator;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.types.JonneObject;

public interface BinaryOperator {

	JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal);

}
