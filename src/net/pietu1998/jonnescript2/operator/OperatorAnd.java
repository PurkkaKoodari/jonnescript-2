package net.pietu1998.jonnescript2.operator;

import java.math.BigInteger;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.types.JonneBoolean;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.jonnescript2.types.JonneNumber;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.NumericValue;
import net.pietu1998.jonnescript2.types.Wrappers;

public class OperatorAnd implements BinaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal) {
		if (leftVal instanceof JonneNull && rightVal instanceof JonneNull) {
			return Wrappers.wrapNull();
		}
		if (leftVal instanceof JonneBoolean && rightVal instanceof JonneBoolean) {
			return andBoolean((JonneBoolean) leftVal, (JonneBoolean) rightVal);
		}
		if (leftVal instanceof NumericValue && rightVal instanceof NumericValue) {
			return andNumeric(leftVal.toJonneNumber(context), rightVal.toJonneNumber(context));
		}
		throw new UndefinedOperatorException();
	}

	private static JonneObject andBoolean(JonneBoolean leftVal, JonneBoolean rightVal) {
		return Wrappers.wrap(leftVal.value && rightVal.value);
	}

	private static JonneObject andNumeric(JonneNumber leftVal, JonneNumber rightVal) {
		try {
			BigInteger leftInt = leftVal.value.toBigIntegerExact();
			BigInteger rightInt = rightVal.value.toBigIntegerExact();
			return Wrappers.wrap(leftInt.and(rightInt));
		} catch (ArithmeticException e) {
			throw new ExecutionException(OperatorType.AND.sourceString + " ei ole määritelty ei-kokonaisluvuille");
		}
	}

}
