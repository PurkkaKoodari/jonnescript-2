package net.pietu1998.jonnescript2.operator;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.jonnescript2.types.JonneNumber;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.NumericValue;
import net.pietu1998.jonnescript2.types.SequenceValue;
import net.pietu1998.jonnescript2.types.Wrappers;

public class OperatorLength implements UnaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject operandVal) {
		if (operandVal instanceof JonneNull) {
			return Wrappers.wrapNull();
		}
		if (operandVal instanceof SequenceValue) {
			return lengthSequence((SequenceValue) operandVal);
		}
		if (operandVal instanceof NumericValue) {
			return lengthNumeric(operandVal.toJonneNumber(context));
		}
		throw new UndefinedOperatorException();
	}

	private static JonneObject lengthSequence(SequenceValue operandVal) {
		return Wrappers.wrap(operandVal.length());
	}

	private static JonneObject lengthNumeric(JonneNumber operandVal) {
		return Wrappers.wrap(operandVal.value.toString().length());
	}

}
