package net.pietu1998.jonnescript2.operator;

import java.util.ArrayList;
import java.util.List;

import net.pietu1998.jonnescript2.Context;
import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.types.JonneArray;
import net.pietu1998.jonnescript2.types.JonneFunctionBase;
import net.pietu1998.jonnescript2.types.JonneFunctionComposition;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.jonnescript2.types.JonneNumber;
import net.pietu1998.jonnescript2.types.JonneString;
import net.pietu1998.jonnescript2.types.JonneObject;
import net.pietu1998.jonnescript2.types.NumericValue;
import net.pietu1998.jonnescript2.types.SequenceValue;
import net.pietu1998.jonnescript2.types.Wrappers;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.unicode.UnicodeStringBuilder;

public class OperatorMultiply implements BinaryOperator {

	@Override
	public JonneObject apply(Context context, JonneObject leftVal, JonneObject rightVal) {
		if (leftVal instanceof JonneNull && rightVal instanceof JonneNull) {
			return Wrappers.wrapNull();
		}
		if (leftVal instanceof NumericValue && rightVal instanceof NumericValue) {
			return multiplyNumeric(leftVal.toJonneNumber(context), rightVal.toJonneNumber(context));
		}
		if (leftVal instanceof NumericValue && rightVal instanceof JonneString) {
			return repeatString(leftVal.toJonneNumber(context), (JonneString) rightVal);
		}
		if (leftVal instanceof JonneString && rightVal instanceof NumericValue) {
			return repeatString(rightVal.toJonneNumber(context), (JonneString) leftVal);
		}
		if (leftVal instanceof NumericValue && rightVal instanceof JonneArray) {
			return repeatArray(leftVal.toJonneNumber(context), (JonneArray) rightVal);
		}
		if (leftVal instanceof JonneArray && rightVal instanceof NumericValue) {
			return repeatArray(rightVal.toJonneNumber(context), (JonneArray) leftVal);
		}
		if (leftVal instanceof JonneString && rightVal instanceof JonneString) {
			return stringCartesianProduct((JonneString) leftVal, (JonneString) rightVal);
		}
		if (leftVal instanceof SequenceValue && rightVal instanceof SequenceValue) {
			return cartesianProduct((SequenceValue) leftVal, (SequenceValue) rightVal);
		}
		if (leftVal instanceof JonneFunctionBase && rightVal instanceof JonneFunctionBase) {
			return new JonneFunctionComposition((JonneFunctionBase) leftVal, (JonneFunctionBase) rightVal);
		}
		throw new UndefinedOperatorException();
	}

	private static JonneObject multiplyNumeric(JonneNumber leftVal, JonneNumber rightVal) {
		return Wrappers.wrap(leftVal.value.multiply(rightVal.value));
	}

	private static JonneObject repeatString(JonneNumber multiplier, JonneString string) {
		try {
			int times = multiplier.value.toBigInteger().intValueExact();
			if (times < 0)
				throw new ExecutionException("negatiivinen merkkijonon kerroin");
			UnicodeStringBuilder result = new UnicodeStringBuilder();
			for (int i = 0; i < times; i++)
				result.append(string.value);
			return Wrappers.wrap(result.toUnicodeString());
		} catch (ArithmeticException e) {
			throw new ExecutionException("liian suuri merkkijonon kerroin");
		}
	}

	private static JonneObject repeatArray(JonneNumber multiplier, JonneArray array) {
		try {
			int times = multiplier.value.toBigInteger().intValueExact();
			if (times < 0)
				throw new ExecutionException("negatiivinen taulukon kerroin");
			List<JonneObject> result = new ArrayList<>();
			for (int i = 0; i < times; i++)
				result.addAll(array.data);
			return Wrappers.wrap(result);
		} catch (ArithmeticException e) {
			throw new ExecutionException("liian suuri taulukon kerroin");
		}
	}

	private static JonneObject stringCartesianProduct(JonneString leftVal, JonneString rightVal) {
		List<JonneObject> result = new ArrayList<>();
		UnicodeString left = leftVal.value;
		UnicodeString right = rightVal.value;
		for (int i = 0; i < leftVal.value.length(); i++) {
			for (int j = 0; j < rightVal.value.length(); j++) {
				result.add(Wrappers.wrap(UnicodeString.fromData(new int[] { left.codePointAt(i), right.codePointAt(j) })));
			}
		}
		return Wrappers.wrap(result);
	}

	private static JonneObject cartesianProduct(SequenceValue leftVal, SequenceValue rightVal) {
		List<JonneObject> result = new ArrayList<>();
		List<JonneObject> left = leftVal.asList();
		List<JonneObject> right = rightVal.asList();
		for (JonneObject aLeft : left) {
			for (JonneObject aRight : right) {
				List<JonneObject> pair = new ArrayList<>();
				pair.add(aLeft);
				pair.add(aRight);
				result.add(Wrappers.wrap(pair));
			}
		}
		return Wrappers.wrap(result);
	}

}
