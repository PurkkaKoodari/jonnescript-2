package net.pietu1998.jonnescript2.lexer;

/**
 * Represents a syntactic token.
 */
public abstract class Token {

	/**
	 * Contains the possible types for {@code Token}s.
	 */
	public enum Type {
		LITERAL_NUMBER("numeroliteraali"),
		LITERAL_BOOLEAN("totuusarvoliteraali"),
		LITERAL_STRING("merkkijonoliteraali"),
		LITERAL_NULL("tyhjäliteraali"),
		IDENTIFIER("tunniste"),
		KEYWORD("avainsana"),
		OPERATOR("operaattori"),
		FUNCTION("toiminto"),
		ARRAY("taulukko"),
		OBJECT("olio"),
		THIS("minä"),
		OPERATOR_NAME("operaattoriviittaus");

		// TODO move localization out of classes
		/**
		 * The user-friendly name of the token type.
		 */
		public final String friendlyName;

		Type(String friendlyName) {
			this.friendlyName = friendlyName;
		}
	}

	/**
	 * The name of the file containing the source for this token. May be
	 * {@code null} if this token isn't read from a file.
	 */
	public String file = null;

	/**
	 * The line number that contains the source for this token. May be -1 if the
	 * location of this token is not known or if it isn't read from a file.
	 */
	public int line = -1;

	/**
	 * Creates a new {@code Token} without setting a location.
	 */
	protected Token() {}

	/**
	 * 
	 * Creates a new {@code Token} and sets the location based on the given
	 * {@link StringToken}.
	 * 
	 * @param location
	 *            the {@link StringToken} to set the location from.
	 */
	protected Token(StringToken location) {
		setLocation(location);
	}

	/**
	 * Returns the type of this token. Must not return {@code null}.
	 * 
	 * @return The type of this token.
	 */
	public abstract Type getType();

	/**
	 * <p>
	 * Indicates whether the data of the given token is equal to the data of
	 * this token.
	 * </p>
	 * <p>
	 * The argument will always be of the same class as this token.
	 * </p>
	 * 
	 * @param token
	 *            the token to compare to this one.
	 * @return {@code true} if and only if the given token has the same data as
	 *         this token, {@code false} otherwise.
	 */
	protected abstract boolean dataEquals(Token token);

	private void setLocation(StringToken location) {
		if (location == null) {
			file = null;
			line = -1;
		} else {
			file = location.file;
			line = location.line;
		}
	}

	/**
	 * Gets a friendly name for this token, for use in error messages.
	 * 
	 * @return A friendly name for this token.
	 */
	public String getFriendlyName() {
		return getType().friendlyName;
	}

	/**
	 * <p>
	 * Indicates whether some object is equal to this token.
	 * </p>
	 * <p>
	 * An object is equal to this token if and only if
	 * </p>
	 * <ul>
	 * <li>it is of the same class as this token and</li>
	 * <li>it has the same data as this token as defined by
	 * {@link #dataEquals(Token)}.</li>
	 * </ul>
	 * 
	 * @param obj
	 *            the reference object with which to compare.
	 * @return {@code true} if this token is equal to the obj argument;
	 *         {@code false} otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return dataEquals((Token) obj);
	}

}
