package net.pietu1998.jonnescript2.lexer;

import java.util.Arrays;

import net.pietu1998.unicode.UnicodeString;

public enum KeywordType {
	CORIANDER("korianteri"),
	MOPED("mopo"),
	WILLIAMS_BROTHER("viljamin veli"),
	IF("jos"),
	ELSE("muuten"),
	UNTIL("kunnes"),
	WHILE("niin kauan kuin"),
	TRY("yritä"),
	CATCH("jos kusit"),
	THROW("heitä"),
	END_BLOCK("lopeta"),
	PARAM_SEPARATOR("sitten"),
	VARARGS("useita"),
	END_PARAM_LIST("valmis"),
	RETURN("palauta"),
	FUNCTION("toiminto"),
	ARRAY("taulukko"),
	OBJECT("olio"),
	THIS("minä"),
	PARENTHESIS("ensin"),
	COMMENT("unohda");

	public final UnicodeString[] sourceParts;
	public final UnicodeString sourceString;

	KeywordType(String source) {
		this.sourceParts = Arrays.stream(source.split(" ")).map(UnicodeString::new)
				.toArray(UnicodeString[]::new);
		this.sourceString = new UnicodeString(source);
	}
}