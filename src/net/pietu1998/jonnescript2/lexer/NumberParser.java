package net.pietu1998.jonnescript2.lexer;

import java.math.BigInteger;

import net.pietu1998.unicode.UnicodeString;

/**
 * Parses numbers represented in Finnish.
 */
public class NumberParser {

	private static final String[] digits = new String[] { "nolla", "yksi", "kaksi", "kolme", "neljä", "viisi", "kuusi",
			"seitsemän", "kahdeksan", "yhdeksän" };
	private static final String[] millionPowers = new String[] { "m", "b", "tr", "kvadr", "kvint", "sekst", "sept", "okt",
			"non", "dek", "undek", "duodek", "tredek", "kvattuordek", "kvindek", "sedek", "septendek", "duodevigint",
			"undevigint", "vigint", "unvigint", "duovigint", "trevigint", "kvattuorvigint", "kvinvigint", "sevigint",
			"septenvigint", "duodetrigint", "undetrigint", "trigint" };

	private static final BigInteger MILLION = BigInteger.valueOf(1000000);

	private final String text;
	private int position = 0;

	private NumberParser(UnicodeString text) {
		this.text = text.toString();
	}

	/**
	 * Attempts to parse the contents of a {@link StringToken} to a
	 * {@code BigInteger}.
	 * 
	 * @param token
	 *            the {@link StringToken} to parse.
	 * @return The numeric value of the {@link StringToken} as a
	 *         {@code BigInteger}, or {@code null} if it can't be parsed.
	 */
	public static BigInteger tryParseNumber(StringToken token) {
		return new NumberParser(token.content).tryParse();
	}

	public static UnicodeString expressNumber(BigInteger number) {
		BigInteger[] result = number.divideAndRemainder(MILLION);
		String string = expressUpToMillion(result[1].intValue(), number.equals(BigInteger.ZERO));
		for (int i = 0; !result[0].equals(BigInteger.ZERO); i++) {
			result = result[0].divideAndRemainder(MILLION);
			int multiplier = result[1].intValue();
			if (multiplier == 1) {
				string = millionPowers[i] + "iljoona" + string;
			} else if (multiplier > 1) {
				string = expressUpToMillion(multiplier, false) + millionPowers[i] + "iljoonaa" + string;
			}
		}
		return new UnicodeString(string);
	}

	private static String expressUpToMillion(int multiplier, boolean allowZero) {
		if (multiplier < 1000)
			return expressUpToThousand(multiplier, allowZero);
		else if (multiplier < 2000)
			return "tuhat" + expressUpToThousand(multiplier % 1000, false);
		else
			return expressUpToThousand(multiplier / 1000, false) + "tuhatta"
					+ expressUpToThousand(multiplier % 1000, false);
	}

	private static String expressUpToThousand(int multiplier, boolean allowZero) {
		if (multiplier < 100)
			return expressUpToHundred(multiplier, allowZero);
		else if (multiplier < 200)
			return "sata" + expressUpToHundred(multiplier % 100, false);
		else
			return digits[multiplier / 100] + "sataa" + expressUpToHundred(multiplier % 100, false);
	}

	private static String expressUpToHundred(int multiplier, boolean allowZero) {
		if (multiplier == 0 && !allowZero)
			return "";
		else if (multiplier < 10)
			return digits[multiplier];
		else if (multiplier == 10)
			return "kymmenen";
		else if (multiplier < 20)
			return digits[multiplier % 10] + "toista";
		else if (multiplier % 10 == 0)
			return digits[multiplier / 10] + "kymmentä";
		else
			return digits[multiplier / 10] + "kymmentä" + digits[multiplier % 10];
	}

	private BigInteger tryParse() {
		if (text.isEmpty())
			return null;
		if (text.equalsIgnoreCase(digits[0]))
			return BigInteger.ZERO;
		BigInteger result = BigInteger.ZERO;
		int maxPower = millionPowers.length - 1;
		mainLoop: while (position < text.length()) {
			for (int i = 0; i < millionPowers.length; i++) {
				if (text.startsWith(millionPowers[i] + "iljoona", position)) {
					if (i > maxPower)
						return null;
					result = result.add(MILLION.pow(i + 1));
					position += millionPowers[i].length() + 7;
					maxPower = i - 1;
					continue mainLoop;
				}
			}
			int multiplier = readMultiplier(true);
			for (int i = 0; i < millionPowers.length; i++) {
				if (text.startsWith(millionPowers[i] + "iljoonaa", position)) {
					if (i > maxPower)
						return null;
					if (multiplier < 2)
						return null;
					result = result.add(MILLION.pow(i + 1).multiply(BigInteger.valueOf(multiplier)));
					position += millionPowers[i].length() + 8;
					maxPower = i - 1;
					continue mainLoop;
				}
			}
			result = result.add(BigInteger.valueOf(multiplier));
			if (position != text.length())
				return null;
		}
		return result;
	}

	private int readMultiplier(boolean withThousands) {
		if (withThousands && text.startsWith("tuhat", position)) {
			position += 5;
			return 1000 + readMultiplier(false);
		}
		int hundreds, tens, ones, result;
		hundreds = readDigit("sata", "sataa");
		tens = readDigit("kymmenen", "kymmentä");
		if (tens == 0) {
			ones = readDigit("yksitoista", "toista");
			if (ones == 0) {
				ones = readDigit(digits[1], "");
			} else {
				ones += 10;
			}
		} else {
			ones = readDigit(digits[1], "");
		}
		result = hundreds * 100 + tens * 10 + ones;
		if (withThousands && text.startsWith("tuhatta", position)) {
			if (result < 2)
				return 0;
			position += 7;
			return result * 1000 + readMultiplier(false);
		} else {
			return result;
		}
	}

	private int readDigit(String single, String plural) {
		if (text.startsWith(single, position)) {
			position += single.length();
			return 1;
		}
		for (int i = 2; i < digits.length; i++) {
			if (text.startsWith(digits[i] + plural, position)) {
				position += digits[i].length() + plural.length();
				return i;
			}
		}
		return 0;
	}
}
