package net.pietu1998.jonnescript2.lexer;


/**
 * A token that represents a language keyword.
 */
public class TokenKeyword extends Token {

	/**
	 * The type of the keyword represented by this {@code TokenKeyword}.
	 */
	public final KeywordType keywordType;

	/**
	 * Creates a new {@code TokenKeyword} with the given keyword type, without
	 * setting a location.
	 * 
	 * @param keywordType
	 *            the keyword type.
	 */
	public TokenKeyword(KeywordType keywordType) {
		this.keywordType = keywordType;
	}

	/**
	 * Creates a new {@code TokenKeyword} with the given keyword type and sets
	 * the location based on the given {@link StringToken}.
	 * 
	 * @param location
	 *            the {@link StringToken} to set the location from.
	 * @param keywordType
	 *            the keyword type.
	 */
	public TokenKeyword(StringToken location, KeywordType keywordType) {
		super(location);
		this.keywordType = keywordType;
	}

	@Override
	public Type getType() {
		return Type.KEYWORD;
	}

	@Override
	public String getFriendlyName() {
		return Type.KEYWORD.friendlyName + " " + keywordType.sourceString;
	}

	@Override
	protected boolean dataEquals(Token other) {
		return keywordType == ((TokenKeyword) other).keywordType;
	}

	@Override
	public String toString() {
		return "TokenKeyword [keywordType=" + keywordType + "]";
	}

}
