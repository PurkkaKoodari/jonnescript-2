package net.pietu1998.jonnescript2.lexer;

import net.pietu1998.unicode.UnicodeString;

/**
 * A token that represents a string literal.
 */
public class TokenLiteralString extends Token {

	/**
	 * The value of the string literal represented by this
	 * {@code TokenLiteralString}.
	 */
	public final UnicodeString value;

	/**
	 * Creates a new {@code TokenLiteralString} with the given string literal
	 * value, without setting a location.
	 * 
	 * @param value
	 *            the string literal value.
	 */
	public TokenLiteralString(UnicodeString value) {
		this.value = value;
	}

	/**
	 * Creates a new {@code TokenLiteralString} with the given string literal
	 * value and sets the location based on the given {@link StringToken}.
	 * 
	 * @param location
	 *            the {@link StringToken} to set the location from.
	 * @param value
	 *            the string literal value.
	 */
	public TokenLiteralString(StringToken location, UnicodeString value) {
		super(location);
		this.value = value;
	}

	@Override
	public Type getType() {
		return Type.LITERAL_STRING;
	}

	@Override
	protected boolean dataEquals(Token other) {
		return value.equals(((TokenLiteralString) other).value);
	}

	@Override
	public String toString() {
		return "TokenLiteralString [value=" + value + "]";
	}

}
