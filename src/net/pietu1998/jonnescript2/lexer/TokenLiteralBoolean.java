package net.pietu1998.jonnescript2.lexer;

/**
 * A token that represents a Boolean literal.
 */
public class TokenLiteralBoolean extends Token {

	/**
	 * The value of the Boolean literal represented by this
	 * {@code TokenLiteralBoolean}.
	 */
	public final boolean value;

	/**
	 * Creates a new {@code TokenLiteralBoolean} with the given Boolean literal value,
	 * without setting a location.
	 * 
	 * @param value
	 *            the Boolean literal value.
	 */
	public TokenLiteralBoolean(boolean value) {
		this.value = value;
	}

	/**
	 * Creates a new {@code TokenLiteralBoolean} with the given Boolean literal
	 * value and sets the location based on the given {@link StringToken}.
	 * 
	 * @param location
	 *            the {@link StringToken} to set the location from.
	 * @param value
	 *            the Boolean literal value.
	 */
	public TokenLiteralBoolean(StringToken location, boolean value) {
		super(location);
		this.value = value;
	}

	@Override
	public Type getType() {
		return Type.LITERAL_BOOLEAN;
	}

	@Override
	protected boolean dataEquals(Token other) {
		return value == ((TokenLiteralBoolean) other).value;
	}

	@Override
	public String toString() {
		return "TokenLiteralBoolean [value=" + value + "]";
	}

}
