package net.pietu1998.jonnescript2.lexer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import net.pietu1998.unicode.UnicodeString;

public class LexerState {

	private Optional<Function<StringToken, Token>> validEnding = Optional.empty();
	private final Map<UnicodeString, LexerState> paths = new HashMap<>();

	public LexerState() {}

	private LexerState(UnicodeString[] tokens, int offset, Function<StringToken, Token> ending) {
		addValidPath(tokens, offset, ending);
	}

	public void addValidPath(UnicodeString[] tokens, Function<StringToken, Token> ending) {
		addValidPath(tokens, 0, ending);
	}

	private void addValidPath(UnicodeString[] tokens, int offset, Function<StringToken, Token> ending) {
		if (offset == tokens.length)
			validEnding = Optional.of(ending);
		else {
			UnicodeString key = tokens[offset].toLowerCase();
			if (paths.containsKey(key))
				paths.get(key).addValidPath(tokens, offset + 1, ending);
			else
				paths.put(key, new LexerState(tokens, offset + 1, ending));
		}
	}

	public Optional<Token> tryParse(Tokenizer tokenizer) throws IOException {
		return tryParse(tokenizer, Optional.empty());
	}

	private Optional<Token> tryParse(Tokenizer tokenizer, Optional<StringToken> start) throws IOException {
		StringToken token = tokenizer.pollToken();
		if (token != null) {
			UnicodeString key = token.content.toLowerCase();
			if (paths.containsKey(key)) {
				Optional<Token> inner = paths.get(key).tryParse(tokenizer, Optional.of(start.orElse(token)));
				if (inner.isPresent())
					return inner;
			}
			tokenizer.returnToken(token);
		}
		return validEnding.map(generator -> generator.apply(start.orElse(token)));
	}

}
