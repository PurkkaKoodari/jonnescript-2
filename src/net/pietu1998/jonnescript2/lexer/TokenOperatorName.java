package net.pietu1998.jonnescript2.lexer;

import net.pietu1998.unicode.UnicodeString;

/**
 * A token that represents the name of an operator.
 */
public class TokenOperatorName extends Token {

	/**
	 * The name of the operator represented by this {@code TokenOperatorName}.
	 */
	public final UnicodeString name;

	/**
	 * Creates a new {@code TokenOperatorName} with the given operator name, without setting a location.
	 * 
	 * @param name
	 *            the operator name.
	 */
	public TokenOperatorName(UnicodeString name) {
		this.name = name;
	}

	/**
	 * Creates a new {@code TokenLiteralNumber} with the given operator name and sets the location based on the given
	 * {@link StringToken}.
	 * 
	 * @param location
	 *            the {@link StringToken} to set the location from.
	 * @param name
	 *            the operator name.
	 */
	public TokenOperatorName(StringToken location, UnicodeString name) {
		super(location);
		this.name = name;
	}

	@Override
	public Type getType() {
		return Type.OPERATOR_NAME;
	}
	
	@Override
	public String getFriendlyName() {
		return Type.OPERATOR_NAME.friendlyName + " " + name;
	}

	@Override
	protected boolean dataEquals(Token token) {
		return name.equals(((TokenOperatorName) token).name);
	}

	@Override
	public String toString() {
		return "TokenOperatorName [name=" + name + "]";
	}

}
