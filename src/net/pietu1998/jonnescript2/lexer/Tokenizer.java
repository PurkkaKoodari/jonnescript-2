package net.pietu1998.jonnescript2.lexer;

import java.io.IOException;
import java.io.Reader;
import java.util.Deque;
import java.util.LinkedList;

import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.unicode.UnicodeStringBuilder;

/**
 * Reads UTF-16 characters from a {@code Reader} and outputs a stream of
 * {@link StringToken}s.
 */
public class Tokenizer {

	private Reader reader;
	private String file;

	private final Deque<StringToken> tokenCache = new LinkedList<>();
	private final Deque<Integer> charCache = new LinkedList<>();

	private UnicodeStringBuilder paddingBuilder = new UnicodeStringBuilder();

	private int line;

	/**
	 * Creates a {@code Tokenizer} that reads characters from the given
	 * {@code Reader}.
	 * 
	 * @param reader
	 *            the {@code Reader} to read characters from.
	 * @param file
	 *            the filename from which the data is being read, or
	 *            {@code null} to not specify a file.
	 */
	public Tokenizer(Reader reader, String file) {
		this.reader = reader;
		this.file = file;
		this.line = 1;
	}

	/**
	 * Changes the {@code Reader} used by this {@code Tokenizer}.
	 * 
	 * @param reader
	 *            the new {@code Reader}.
	 * @return The old {@code Reader}, which can then be closed.
	 */
	public Reader setReader(Reader reader) {
		Reader old = this.reader;
		this.reader = reader;
		return old;
	}

	/**
	 * Clears the cache of this {@code Tokenizer}.
	 */
	public void resetCache() {
		charCache.clear();
		tokenCache.clear();
		paddingBuilder = new UnicodeStringBuilder();
	}

	/**
	 * Resets the line number of this {@code Tokenizer} to 1.
	 */
	public void resetLine() {
		this.line = 1;
	}

	private int readCodePoint() throws IOException {
		int first = reader.read();
		if (first == -1) {
			return -1;
		} else if (Character.isHighSurrogate((char) first)) {
			int second = reader.read();
			if (second == -1)
				throw new IOException("invalid character hit while decoding");
			else if (Character.isLowSurrogate((char) second))
				return Character.toCodePoint((char) first, (char) second);
			else
				throw new IOException("invalid character hit while decoding");
		} else {
			return first;
		}
	}

	private int pollCodePoint() throws IOException {
		if (!tokenCache.isEmpty())
			throw new IllegalStateException("token cache must be cleared before reading characters");
		if (charCache.isEmpty())
			charCache.addLast(readCodePoint());
		int character = charCache.pollFirst();
		if (character == '\n')
			line++;
		return character;
	}

	private int pollNormalizedCodePoint() throws IOException {
		int character = pollCodePoint();
		if (character == '\r') {
			int next = pollCodePoint();
			if (next != '\n')
				returnCharacter(next);
			return '\n';
		}
		return character;
	}

	private void returnCharacter(int character) {
		charCache.addFirst(character);
		if (character == '\n')
			line--;
	}

	/**
	 * Gets the next {@link StringToken} in the data.
	 * 
	 * @return The {@link StringToken} read, or {@code null} if no string token
	 *         is found.
	 * @throws IOException
	 *             if the underlying {@code Reader} throws an
	 *             {@code IOException}.
	 */
	public StringToken pollToken() throws IOException {
		if (tokenCache.isEmpty()) {
			int read;
			UnicodeStringBuilder contentBuilder = new UnicodeStringBuilder();
			read = pollNormalizedCodePoint();
			while (read != -1 && !Character.isLetter(read)) {
				paddingBuilder.append(read);
				read = pollNormalizedCodePoint();
			}
			if (read == -1)
				return null;
			contentBuilder.append(read);
			read = pollNormalizedCodePoint();
			while (read != -1 && Character.isLetter(read)) {
				contentBuilder.append(read);
				read = pollNormalizedCodePoint();
			}
			if (read != -1)
				returnCharacter(read);
			UnicodeString padding = paddingBuilder.toUnicodeString();
			UnicodeString content = contentBuilder.toUnicodeString();
			tokenCache.addLast(new StringToken(padding, content, file, line));
			paddingBuilder = new UnicodeStringBuilder();
		}
		return tokenCache.removeFirst();
	}

	/**
	 * Returns a {@link StringToken} to the queue.
	 * 
	 * @param token
	 *            the {@link StringToken} to return.
	 */
	public void returnToken(StringToken token) {
		tokenCache.addFirst(token);
	}

	/**
	 * Gets and optionally clears any padding that has already been read and is
	 * waiting for the next token.
	 * 
	 * @return Any cached padding characters.
	 */
	public UnicodeString getCachedPadding(boolean clear) {
		UnicodeString padding = paddingBuilder.toUnicodeString();
		if (clear)
			paddingBuilder = new UnicodeStringBuilder();
		return padding;
	}

}
