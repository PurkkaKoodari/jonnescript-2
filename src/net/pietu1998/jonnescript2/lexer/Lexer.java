package net.pietu1998.jonnescript2.lexer;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Optional;
import java.util.function.Supplier;

import net.pietu1998.jonnescript2.exception.ParseException;
import net.pietu1998.jonnescript2.operator.Arity;
import net.pietu1998.jonnescript2.operator.OperatorType;
import net.pietu1998.jonnescript2.operator.OverrideMode;
import net.pietu1998.jonnescript2.types.JonneBoolean;
import net.pietu1998.jonnescript2.types.JonneNull;
import net.pietu1998.unicode.UnicodeString;
import net.pietu1998.unicode.UnicodeStringBuilder;

/**
 * Takes a stream of {@link StringToken}s provided by a {@link Tokenizer} and
 * converts it into a stream of {@link Token} s as defined by JonneScript 2.
 */
public class Lexer {

	/**
	 * <p>
	 * Represents an object that can be requested to acquire more input.
	 * </p>
	 * <p>
	 * When {@link #requestInputContinuation()} is called, it should block until
	 * one of the following states is reached:
	 * </p>
	 * <ul>
	 * <li>The creator of the {@code InputContinuationRequest} is ready to
	 * supply more input.</li>
	 * <li>The creator of the {@code InputContinuationRequest} is certain that
	 * the input has ended, and more input should not be requested before
	 * returning control to the creator.</li>
	 * </ul>
	 * <p>
	 * This is a functional interface whose functional method is
	 * {@link #requestInputContinuation()}.
	 * </p>
	 */
	public interface InputContinuationRequest {
		/**
		 * Requests more input.
		 * 
		 * @throws IOException
		 * 	           if requesting more input causes an IO exception.
		 */
		void requestInputContinuation() throws IOException;
	}

	private Tokenizer tokenizer;
	private Optional<Token> current = null;

	private InputContinuationRequest continuationRequest = null;

	private static final LexerState rootState = new LexerState();

	// TODO move language definitions out of classes
	public static final UnicodeString BEGIN_STRING = new UnicodeString("teksti");
	public static final UnicodeString ESCAPE = new UnicodeString("kusetin");

	static {
		for (KeywordType keywordType : KeywordType.values()) {
			rootState.addValidPath(keywordType.sourceParts, start -> new TokenKeyword(start, keywordType));
		}
		for (OperatorType operatorType : OperatorType.values()) {
			rootState.addValidPath(operatorType.sourceParts, start -> new TokenOperator(start, operatorType));
		}
		rootState.addValidPath(new UnicodeString[] { JonneNull.STRING_NULL }, TokenLiteralNull::new);
		rootState.addValidPath(new UnicodeString[] { JonneBoolean.STRING_TRUE }, start -> new TokenLiteralBoolean(
				start, true));
		rootState.addValidPath(new UnicodeString[] { JonneBoolean.STRING_FALSE }, start -> new TokenLiteralBoolean(
				start, false));
	}

	/**
	 * Creates a {@code Lexer} with the given {@link Tokenizer} and no
	 * {@link InputContinuationRequest}.
	 * 
	 * @param tokenizer
	 *            the tokenizer to poll {@link StringToken}s from.
	 */
	public Lexer(Tokenizer tokenizer) {
		this.tokenizer = tokenizer;
	}

	/**
	 * Creates a {@code Lexer} with the given {@link Tokenizer} and
	 * {@link InputContinuationRequest}.
	 * 
	 * @param tokenizer
	 *            the tokenizer to poll {@link StringToken}s from.
	 * @param continuationRequest
	 *            the {@link InputContinuationRequest} to query when a required
	 *            token is missing.
	 */
	public Lexer(Tokenizer tokenizer, InputContinuationRequest continuationRequest) {
		this(tokenizer);
		this.continuationRequest = continuationRequest;
	}

	/**
	 * Returns an {@code Optional} containing the last {@link Token} the lexer
	 * has advanced to, or an empty {@code Optional} if none are found.
	 * Initially returns an empty {@code Optional} until {@link #next()} is
	 * called for the first time.
	 * 
	 * @return The current {@link Token}, or an empty {@code Optional} if none
	 */
	public Optional<Token> peek() {
		return current;
	}

	/**
	 * Indicates whether this {@code Lexer} has a token, i.e. whether a call to
	 * {@link #peek()} will return a non-empty {@code Optional}.
	 * 
	 * @return {@code true} if and only if this {@code Lexer} has a token,
	 *         {@code false} otherwise
	 */
	public boolean hasToken() {
		return current.isPresent();
	}

	/**
	 * <p>
	 * Advances to the next {@link Token}.
	 * </p>
	 * <p>
	 * If the tokenizer runs out of tokens and the lexer is in a state where an
	 * end-of-file is not valid, the lexer may request more input via its
	 * {@link InputContinuationRequest}, if any, which may cause this method to
	 * block.
	 * </p>
	 * 
	 * @throws ParseException
	 *             if a syntax error is encountered.
	 * @throws IOException
	 *             if the underlying {@link Tokenizer} throws an
	 *             {@code IOException}.
	 * @see Tokenizer#pollToken()
	 * @see InputContinuationRequest
	 */
	public void next() throws ParseException, IOException {
		current = parseToken();
	}

	/**
	 * Attempts to request more input via the lexer's
	 * {@link InputContinuationRequest}, if any.
	 * 
	 * @throws IOException
	 *             if invoking the {@code InputContinuationRequest} causes an
	 *             {@code IOException}.
	 */
	public void requestInputContinuation() throws IOException {
		if (continuationRequest != null)
			continuationRequest.requestInputContinuation();
	}

	private StringToken getRequiredToken(Supplier<ParseException> error) throws IOException, ParseException {
		StringToken token = tokenizer.pollToken();
		if (token == null) {
			requestInputContinuation();
			token = tokenizer.pollToken();
			if (token == null)
				throw error.get();
		}
		return token;
	}

	private Optional<Token> parseToken() throws IOException, ParseException {
		StringToken token = tokenizer.pollToken();
		if (token == null)
			return Optional.empty();
		tokenizer.returnToken(token);
		Optional<Token> fromState = rootState.tryParse(tokenizer);
		if (fromState.isPresent())
			return fromState;
		token = tokenizer.pollToken();
		if (token == null)
			return Optional.empty();
		if (token.content.equalsIgnoreCase(Arity.UNARY.friendlyName))
			return Optional.of(tryParseOperator(token, Arity.UNARY));
		if (token.content.equalsIgnoreCase(Arity.BINARY.friendlyName))
			return Optional.of(tryParseOperator(token, Arity.BINARY));
		if (token.content.equalsIgnoreCase(BEGIN_STRING))
			return Optional.of(tryParseString(token));
		BigInteger asNumber = NumberParser.tryParseNumber(token);
		if (asNumber != null)
			return Optional.of(new TokenLiteralNumber(token, asNumber));
		return Optional.of(new TokenIdentifier(token, token.content));
	}

	private TokenOperatorName tryParseOperator(StringToken typeToken, Arity arity) throws ParseException, IOException {
		boolean write = false;
		TokenOperator parsed = tryParseOperatorInner(typeToken, arity);
		if (parsed.operatorType == OperatorType.ASSIGN) {
			write = true;
			parsed = tryParseOperatorInner(typeToken, arity);
		}
		if (!parsed.operatorType.arity.contains(arity))
			throw new ParseException(parsed.getType().friendlyName + " ei ole " + arity.friendlyName + "operaattori",
					parsed);
		if (parsed.operatorType.overrideMode == OverrideMode.NOT_OVERRIDABLE)
			throw new ParseException(arity.friendlyName + "operaattoria " + parsed.operatorType.sourceString
					+ " ei voi korvata");
		if (write && parsed.operatorType.overrideMode == OverrideMode.READ)
			throw new ParseException(arity.friendlyName + "operaattoriin " + parsed.operatorType.sourceString
					+ " ei voi kirjoittaa");
		return new TokenOperatorName(typeToken, parsed.operatorType.methodName(arity, write));
	}

	private TokenOperator tryParseOperatorInner(StringToken typeToken, Arity arity) throws ParseException, IOException {
		Token parsed = rootState.tryParse(tokenizer).orElseThrow(
				() -> new ParseException("operaattori puuttuu", typeToken));
		if (!(parsed instanceof TokenOperator))
			throw new ParseException(parsed.getType().friendlyName + " ei ole " + arity.friendlyName + "operaattori",
					parsed);
		return (TokenOperator) parsed;
	}

	private TokenLiteralString tryParseString(StringToken start) throws ParseException, IOException {
		StringToken token = getRequiredToken(() -> new ParseException("sulkematon merkkijono", start));
		UnicodeStringBuilder builder = new UnicodeStringBuilder();
		int offset = 1;
		while (true) {
			if (token.content.equalsIgnoreCase(KeywordType.END_BLOCK.sourceString)) {
				if (token.padding.length() > offset)
					builder.append(token.padding.substring(offset, token.padding.length() - 1));
				return new TokenLiteralString(start, builder.toUnicodeString());
			} else if (token.content.equalsIgnoreCase(ESCAPE)) {
				if (token.padding.length() > offset)
					builder.append(token.padding.substring(offset, token.padding.length() - 1));
				token = getRequiredToken(() -> new ParseException("sulkematon merkkijono", start));
				builder.append(token.content);
				offset = 1;
			} else {
				if (token.padding.length() > offset)
					builder.append(token.padding.substring(offset, token.padding.length()));
				builder.append(token.content);
				offset = 0;
			}
			token = getRequiredToken(() -> new ParseException("sulkematon merkkijono", start));
		}
	}

	public static boolean isTokenSpecialInString(StringToken token) {
		return token.content.equalsIgnoreCase(KeywordType.END_BLOCK.sourceString)
				|| token.content.equalsIgnoreCase(Lexer.ESCAPE);
	}

}
