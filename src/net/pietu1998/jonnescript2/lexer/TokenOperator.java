package net.pietu1998.jonnescript2.lexer;

import net.pietu1998.jonnescript2.operator.OperatorType;

/**
 * A token that represents an operator.
 */
public class TokenOperator extends Token {

	/**
	 * The type of the operator represented by this {@code TokenOperator}.
	 */
	public final OperatorType operatorType;

	/**
	 * Creates a new {@code TokenOperator} with the given operator type, without
	 * setting a location.
	 * 
	 * @param operatorType
	 *            the operator type.
	 */
	public TokenOperator(OperatorType operatorType) {
		this.operatorType = operatorType;
	}

	/**
	 * Creates a new {@code TokenOperator} with the given operator type and sets
	 * the location based on the given {@link StringToken}.
	 * 
	 * @param location
	 *            the {@link StringToken} to set the location from.
	 * @param operatorType
	 *            the operator type.
	 */
	public TokenOperator(StringToken location, OperatorType operatorType) {
		super(location);
		this.operatorType = operatorType;
	}

	@Override
	public Type getType() {
		return Type.OPERATOR;
	}

	@Override
	public String getFriendlyName() {
		return Type.OPERATOR.friendlyName + " " + operatorType.sourceString;
	}
	
	@Override
	protected boolean dataEquals(Token other) {
		return operatorType == ((TokenOperator) other).operatorType;
	}

	@Override
	public String toString() {
		return "TokenOperator [operatorType=" + operatorType + "]";
	}

}
