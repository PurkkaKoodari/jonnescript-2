package net.pietu1998.jonnescript2.lexer;

import net.pietu1998.unicode.UnicodeString;

/**
 * A token that represents an identifier.
 */
public class TokenIdentifier extends Token {

	/**
	 * The identifier name represented by this {@code TokenIdentifier}.
	 */
	public final UnicodeString name;

	/**
	 * Creates a new {@code TokenIdentifier} with the given identifier name,
	 * without setting a location.
	 * 
	 * @param name
	 *            the identifier name.
	 */
	public TokenIdentifier(UnicodeString name) {
		this.name = name;
	}

	/**
	 * Creates a new {@code TokenIdentifier} with the given identifier name and
	 * sets the location based on the given {@link StringToken}.
	 * 
	 * @param location
	 *            the {@link StringToken} to set the location from.
	 * @param name
	 *            the identifier name.
	 */
	public TokenIdentifier(StringToken location, UnicodeString name) {
		super(location);
		this.name = name;
	}

	@Override
	public Type getType() {
		return Type.IDENTIFIER;
	}
	
	@Override
	public String getFriendlyName() {
		return Type.IDENTIFIER.friendlyName + " \"" + name + "\"";
	}

	@Override
	protected boolean dataEquals(Token other) {
		return name.equalsIgnoreCase(((TokenIdentifier) other).name);
	}

	@Override
	public String toString() {
		return "TokenIdentifier [name=" + name + "]";
	}

}
