package net.pietu1998.jonnescript2.lexer;

/**
 * A token that represents a null literal.
 */
public class TokenLiteralNull extends Token {

	/**
	 * Creates a new {@code TokenLiteralNull} without setting a location.
	 */
	public TokenLiteralNull() {}

	/**
	 * Creates a new {@code TokenLiteralNull} and sets the location based on the
	 * given {@link StringToken}.
	 * 
	 * @param location
	 *            the {@link StringToken} to set the location from.
	 */
	public TokenLiteralNull(StringToken location) {
		super(location);
	}

	@Override
	public Type getType() {
		return Type.LITERAL_NULL;
	}

	@Override
	protected boolean dataEquals(Token other) {
		return true;
	}

	@Override
	public String toString() {
		return "TokenLiteralNull []";
	}
}
