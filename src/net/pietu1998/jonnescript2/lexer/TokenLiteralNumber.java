package net.pietu1998.jonnescript2.lexer;

import java.math.BigInteger;

/**
 * A token that represents a numeric literal.
 */
public class TokenLiteralNumber extends Token {

	/**
	 * The value of the numeric literal represented by this
	 * {@code TokenLiteralNumber}.
	 */
	public final BigInteger value;

	/**
	 * Creates a new {@code TokenLiteralNumber} with the given numeric literal
	 * value, without setting a location.
	 * 
	 * @param value
	 *            the numeric literal value.
	 */
	public TokenLiteralNumber(BigInteger value) {
		this.value = value;
	}

	/**
	 * Creates a new {@code TokenLiteralNumber} with the given numeric literal
	 * value and sets the location based on the given {@link StringToken}.
	 * 
	 * @param location
	 *            the {@link StringToken} to set the location from.
	 * @param value
	 *            the numeric literal value.
	 */
	public TokenLiteralNumber(StringToken location, BigInteger value) {
		super(location);
		this.value = value;
	}

	@Override
	public Type getType() {
		return Type.LITERAL_NUMBER;
	}

	@Override
	protected boolean dataEquals(Token other) {
		return value.equals(((TokenLiteralNumber) other).value);
	}

	@Override
	public String toString() {
		return "TokenLiteralNumber [value=" + value + "]";
	}

}
