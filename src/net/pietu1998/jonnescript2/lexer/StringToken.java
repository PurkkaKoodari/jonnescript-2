package net.pietu1998.jonnescript2.lexer;

import net.pietu1998.unicode.UnicodeString;

/**
 * Represents a string token.
 * 
 * A string token is defined to be a sequence of characters with the Letter
 * category in Unicode. A {@code StringToken} contains the 
 */
public class StringToken {

	public final UnicodeString padding, content;
	public final String file;
	public final int line;

	public StringToken(UnicodeString padding, UnicodeString content, String file, int line) {
		this.padding = padding;
		this.content = content;
		this.file = file;
		this.line = line;
	}

	@Override
	public String toString() {
		return "StringToken [padding=" + padding + ", content=" + content + "]";
	}

}
