package net.pietu1998.jonnescript2;

import java.io.IOException;
import java.io.InputStreamReader;

import net.pietu1998.jonnescript2.exception.ExecutionException;
import net.pietu1998.jonnescript2.types.JonneFunctionNative;
import net.pietu1998.jonnescript2.types.Wrappers;

public class NativeFunctions {

	public static final String LINE_SEPARATOR = System.getProperty("line.separator");

	public static final JonneFunctionNative print = new JonneFunctionNative("tulosta", -1, (context, args) -> {
		for (int i = 0; i < args.length - 1; i++) {
			System.out.print(args[i].toJonneString(context).value);
			System.out.print(' ');
		}
		System.out.println(args[args.length - 1].toJonneString(context).value);
		return Wrappers.wrapNull();
	});

	public static final JonneFunctionNative input = new JonneFunctionNative("lue", 0, (context, args) -> {
		try {
			StringBuilder sb = new StringBuilder();
			InputStreamReader reader = new InputStreamReader(System.in);
			int read = reader.read();
			while (read != -1) {
				sb.append((char) read);
				if (sb.toString().endsWith(LINE_SEPARATOR))
					break;
				read = reader.read();
			}
			String input = sb.toString();
			if (input.endsWith(LINE_SEPARATOR))
				input = input.substring(0, input.length() - LINE_SEPARATOR.length());
			return Wrappers.wrap(input);
		} catch (IOException e) {
			throw new ExecutionException("lukeminen epäonnistui");
		}
	});

	public static final JonneFunctionNative exit = new JonneFunctionNative("poistu", 0, 1, (context, args) -> {
		if (args.length > 0) {
			try {
				System.exit(args[0].toJonneNumber(context).value.intValueExact());
			} catch (ArithmeticException e) {
				throw new ExecutionException("poistumiskoodin täytyy olla kokonaisluku");
			}
		} else
			System.exit(0);
		return Wrappers.wrapNull();
	});

}
