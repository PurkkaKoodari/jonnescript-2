package net.pietu1998.jonnescript2;

import java.util.Optional;

import net.pietu1998.jonnescript2.types.JonneObject;

public class NodeExecutionResult {

	public final boolean returned;
	public final Optional<JonneObject> result;

	private static final NodeExecutionResult empty = new NodeExecutionResult();

	private NodeExecutionResult() {
		this.returned = false;
		this.result = Optional.empty();
	}

	private NodeExecutionResult(boolean returned, JonneObject result) {
		this.returned = returned;
		this.result = Optional.of(result);
	}

	public static NodeExecutionResult of(JonneObject result) {
		return new NodeExecutionResult(false, result);
	}

	public static NodeExecutionResult ofReturned(JonneObject result) {
		return new NodeExecutionResult(true, result);
	}

	public static NodeExecutionResult empty() {
		return empty;
	}

	public Optional<JonneObject> getReturnValue() {
		return returned ? result : Optional.empty();
	}

}
