package net.pietu1998.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;

/**
 * Dumps objects to {@code Writer}s or {@code OutputStreams} in a human-readable
 * indented fashion.
 */
public class DumpWriter {

	private Target inner;
	private int indent = 0;
	private String indentString;
	private boolean newLine = true;

	/**
	 * <p>
	 * Represents an action that dumps data to a {@code DumpWriter}.
	 * </p>
	 * <p>
	 * This is a functional interface whose functional method is {@link #dump()}
	 * .
	 * </p>
	 */
	public interface DumpTask {
		/**
		 * Dumps some data.
		 */
		void dump();
	}

	/**
	 * Creates a {@code DumpWriter} that writes to the given {@code PrintStream}
	 * , with the default indentation string of {@code " "}.
	 *
	 * @param inner
	 *            the {@code PrintStream} to write to.
	 */
	public DumpWriter(PrintStream inner) {
		this(inner, " ");
	}

	/**
	 * Creates a {@code DumpWriter} that writes to the given
	 * {@code OutputStream}, with the default indentation string of {@code " "}
	 * .
	 *
	 * @param inner
	 *            the {@code OutputStream} to write to.
	 */
	public DumpWriter(OutputStream inner) {
		this(inner, " ");
	}

	/**
	 * Creates a {@code DumpWriter} that writes to the given {@code PrintWriter}
	 * , with the default indentation string of {@code " "}.
	 *
	 * @param inner
	 *            the {@code PrintWriter} to write to.
	 */
	public DumpWriter(PrintWriter inner) {
		this(inner, " ");
	}

	/**
	 * Creates a {@code DumpWriter} that writes to the given {@code Writer},
	 * with the default indentation string of {@code " "}.
	 *
	 * @param inner
	 *            the {@code Writer} to write to.
	 */
	public DumpWriter(Writer inner) {
		this(inner, " ");
	}

	/**
	 * Creates a {@code DumpWriter} that writes to the given {@code PrintStream}
	 * with the given indentation string.
	 *
	 * @param inner
	 *            the {@code PrintStream} to write to.
	 * @param indentString
	 *            the string to indent lines with.
	 */
	public DumpWriter(PrintStream inner, String indentString) {
		this.inner = new StreamTarget(inner);
		this.indentString = indentString;
	}

	/**
	 * Creates a {@code DumpWriter} that writes to the given
	 * {@code OutputStream} with the given indentation string.
	 *
	 * @param inner
	 *            the {@code OutputStream} to write to.
	 * @param indentString
	 *            the string to indent lines with.
	 */
	public DumpWriter(OutputStream inner, String indentString) {
		this.inner = new StreamTarget(inner);
		this.indentString = indentString;
	}

	/**
	 * Creates a {@code DumpWriter} that writes to the given {@code PrintWriter}
	 * with the given indentation string.
	 *
	 * @param inner
	 *            the {@code PrintWriter} to write to.
	 * @param indentString
	 *            the string to indent lines with.
	 */
	public DumpWriter(PrintWriter inner, String indentString) {
		this.inner = new WriterTarget(inner);
		this.indentString = indentString;
	}

	/**
	 * Creates a {@code DumpWriter} that writes to the given {@code Writer} with
	 * the given indentation string.
	 *
	 * @param inner
	 *            the {@code Writer} to write to.
	 * @param indentString
	 *            the string to indent lines with.
	 */
	public DumpWriter(Writer inner, String indentString) {
		this.inner = new WriterTarget(inner);
		this.indentString = indentString;
	}

	/**
	 * Prints the system's default line separator to the output. If at the start
	 * of a line, also prints indentation.
	 */
	public void println() {
		doIndent();
		inner.println();
		newLine = true;
	}

	/**
	 * Prints the given string followed system's default line separator to the
	 * output. If at the start of a line, also prints indentation.
	 *
	 * @param s
	 *            the string to print.
	 */
	public void println(String s) {
		doIndent();
		inner.println(s);
		newLine = true;
	}

	/**
	 * Prints the given string to the output. If at the start of a line, also
	 * prints indentation.
	 *
	 * @param s
	 *            the string to print.
	 */
	public void print(String s) {
		doIndent();
		inner.print(s);
	}

	/**
	 * Increments the indentation level of following lines.
	 */
	public void indent() {
		indent++;
	}

	/**
	 * Decrements the indentation level of following lines.
	 *
	 * @throws IllegalStateException
	 *             if the indentation level would go negative because of this
	 *             call.
	 */
	public void dedent() {
		if (indent == 0)
			throw new IllegalStateException("indent changed below zero");
		indent--;
	}

	/**
	 * Asks the given {@code Dumpable} to dump itself to this {@code DumpWriter}
	 * .
	 *
	 * @param dumpable
	 *            the {@code Dumpable} to dump.
	 */
	public void dump(Dumpable dumpable) {
		dumpable.dump(this);
	}

	/**
	 * Executes the given {@code DumpTask} with an indentation level one deeper
	 * than the current one.
	 *
	 * @param task
	 *            the {@code DumpTask} to be executed.
	 */
	public void doIndented(DumpTask task) {
		indent();
		task.dump();
		dedent();
	}

	/**
	 * Asks the given {@code Dumpable} to dump itself to this {@code DumpWriter}
	 * with an indentation level one deeper than the current one.
	 *
	 * @param dumpable
	 *            the {@code Dumpable} to dump.
	 */
	public void dumpIndented(Dumpable dumpable) {
		indent();
		dumpable.dump(this);
		dedent();
	}

	private void doIndent() {
		if (newLine) {
			for (int i = 0; i < indent; i++)
				inner.print(indentString);
			newLine = false;
		}
	}

	/**
	 * Flushes the output.
	 *
	 * @throws IOException
	 *             if flushing the underlying output causes an
	 *             {@code IOException}.
	 */
	public void flush() throws IOException {
		inner.flush();
	}

	/**
	 * Closes this {@code DumpWriter} and the output.
	 *
	 * @throws IOException
	 *             if closing the underlying output causes an
	 *             {@code IOException}.
	 */
	public void close() throws IOException {
		inner.close();
	}

	private interface Target {
		void print(String s);

		void println();

		void println(String s);

		void flush();

		void close();
	}

	private static class StreamTarget implements Target {
		private PrintStream stream;

		public StreamTarget(PrintStream stream) {
			this.stream = stream;
		}

		public StreamTarget(OutputStream stream) {
			this.stream = new PrintStream(stream);
		}

		@Override
		public void println(String s) {
			stream.println(s);
		}

		@Override
		public void println() {
			stream.println();
		}

		@Override
		public void print(String s) {
			stream.print(s);
		}

		@Override
		public void flush() {
			stream.close();
		}

		@Override
		public void close() {
			stream.close();
		}
	}

	private static class WriterTarget implements Target {
		private PrintWriter writer;

		public WriterTarget(PrintWriter writer) {
			this.writer = writer;
		}

		public WriterTarget(Writer writer) {
			this.writer = new PrintWriter(writer);
		}

		@Override
		public void println(String s) {
			writer.println(s);
		}

		@Override
		public void println() {
			writer.println();
		}

		@Override
		public void print(String s) {
			writer.print(s);
		}

		@Override
		public void flush() {
			writer.close();
		}

		@Override
		public void close() {
			writer.close();
		}
	}

}
