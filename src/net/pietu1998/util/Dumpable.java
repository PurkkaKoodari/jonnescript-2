package net.pietu1998.util;

/**
 * Represents an object that can be dumped with a {@link DumpWriter}.
 */
public interface Dumpable {

	/**
	 * Dumps this object's representation to the given {@code DumpWriter}.
	 * 
	 * @param writer
	 *            the {@code DumpWriter} to dump this object to.
	 */
	void dump(DumpWriter writer);

}
