package net.pietu1998.util;

/**
 * Represents a pair of objects.
 *
 * @param <A>
 *            the type of the first object.
 * @param <B>
 *            the type of the second object.
 */
public class Pair<A, B> {
	private final A first;
	private final B second;

	private Pair(A first, B second) {
		this.first = first;
		this.second = second;
	}

	/**
	 * Constructs a new pair from the given objects.
	 * 
	 * @param first
	 *            the first object in the new pair.
	 * @param second
	 *            the second object in the new pair.
	 * @return The new pair.
	 */
	public static <A, B> Pair<A, B> of(A first, B second) {
		return new Pair<>(first, second);
	}

	/**
	 * Returns the first object in this pair.
	 * 
	 * @return The first object in this pair.
	 */
	public A getFirst() {
		return first;
	}

	/**
	 * Returns the second object in this pair.
	 * 
	 * @return The second object in this pair.
	 */
	public B getSecond() {
		return second;
	}

}
