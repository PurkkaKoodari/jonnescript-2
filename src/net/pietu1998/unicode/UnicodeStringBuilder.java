package net.pietu1998.unicode;

import java.util.Arrays;

public class UnicodeStringBuilder {

	private int[] data;
	private int length;

	public UnicodeStringBuilder() {
		this(16);
	}

	public UnicodeStringBuilder(int capacity) {
		this.data = new int[capacity];
		this.length = 0;
	}

	public UnicodeStringBuilder append(int codePoint) {
		if (!UnicodeString.isValidCodePoint(codePoint)) {
			long unsigned = (codePoint + (1L << 31)) % (1L << 32);
			throw new UnicodeException(String.format("invalid character %#04x", unsigned));
		}
		ensureCapacity(length + 1);
		data[length++] = codePoint;
		return this;
	}

	public UnicodeStringBuilder append(String string) {
		return append(new UnicodeString(string));
	}

	public UnicodeStringBuilder append(UnicodeString string) {
		int len = string.length();
		ensureCapacity(length + len);
		string.getCodePoints(data, 0, len, length);
		length += len;
		return this;
	}

	private void ensureCapacity(int size) {
		int newSize = data.length;
		while (size > newSize)
			newSize *= 2;
		data = Arrays.copyOf(data, newSize);
	}

	public int length() {
		return length;
	}

	public UnicodeString toUnicodeString() {
		return UnicodeString.fromData(data, 0, length);
	}

	@Override
	public String toString() {
		return toUnicodeString().toString();
	}

}
