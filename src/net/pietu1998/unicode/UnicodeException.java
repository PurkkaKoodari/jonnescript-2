package net.pietu1998.unicode;

public class UnicodeException extends RuntimeException {

	public UnicodeException() {}

	public UnicodeException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnicodeException(String message) {
		super(message);
	}

	public UnicodeException(Throwable cause) {
		super(cause);
	}

}
