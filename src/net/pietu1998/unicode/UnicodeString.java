package net.pietu1998.unicode;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Represents a string of Unicode characters stored as UTF-32.
 * 
 * @author Pietu1998
 */
public class UnicodeString implements Comparable<UnicodeString> {

	private final int[] data;
	private int hash = 0;

	private UnicodeString(int[] data) {
		this.data = data;
	}

	/**
	 * Initializes a {@code UnicodeString} with empty contents.
	 */
	public UnicodeString() {
		this.data = new int[0];
	}

	/**
	 * Creates a {@code UnicodeString} with the same contents as the given
	 * {@code String}.
	 * 
	 * @param data
	 *            the contents of the new {@code UnicodeString}
	 * @throws UnicodeException
	 *             if the given {@code String} contains invalid characters
	 * @throws IllegalArgumentException
	 *             if {@code null} is given as the argument.
	 * @see UnicodeString#isValidCodePoint(int)
	 */
	public UnicodeString(String data) {
		if (data == null)
			throw new IllegalArgumentException("data can't be null");
		this.data = decodeUtf16(data.toCharArray());
	}

	/**
	 * Creates a {@code UnicodeString} with the same contents as the given
	 * {@code UnicodeString}.
	 * 
	 * @param data
	 *            the contents of the new {@code UnicodeString}
	 * @throws IllegalArgumentException
	 *             if {@code null} is given as the argument.
	 */
	public UnicodeString(UnicodeString data) {
		if (data == null)
			throw new IllegalArgumentException("data can't be null");
		this.data = data.data;
	}

	/**
	 * <p>
	 * Creates a {@code UnicodeString} with the given sequence of code points as
	 * its contents.
	 * </p>
	 * 
	 * <p>
	 * {@code fromData(array)} is equivalent to
	 * {@code fromData(array, 0, array.length)}.
	 * </p>
	 * 
	 * @param data
	 *            the contents of the new {@code UnicodeString}
	 * @return the new {@code UnicodeString}
	 * @throws UnicodeException
	 *             if the given sequence contains invalid code points
	 * @throws IllegalArgumentException
	 *             if {@code null} is given as the argument.
	 * @see UnicodeString#isValidCodePoint(int)
	 */
	public static UnicodeString fromData(int[] data) {
		if (data == null)
			throw new IllegalArgumentException("data can't be null");
		return fromData(data, 0, data.length);
	}

	/**
	 * Creates a {@code UnicodeString} with the given sequence of code points as
	 * its contents.
	 * 
	 * @param data
	 *            the sequence to take code points from
	 * @param offset
	 *            the index of the first code point to include
	 * @param length
	 *            the number of code points to include
	 * @throws UnicodeException
	 *             if the given sequence contains invalid code points
	 * @throws IllegalArgumentException
	 *             if {@code null} is given as the {@code data} argument.
	 * @see UnicodeString#isValidCodePoint(int)
	 */
	public static UnicodeString fromData(int[] data, int offset, int length) {
		if (data == null)
			throw new IllegalArgumentException("data can't be null");
		for (int i = offset; i < offset + length; i++) {
			if (!isValidCodePoint(data[i])) {
				long unsigned = (data[i] + (1L << 31)) % (1L << 32);
				throw new UnicodeException(String.format("invalid character %#04x", unsigned));
			}
		}
		return new UnicodeString(Arrays.copyOfRange(data, offset, offset + length));

	}

	/**
	 * Returns if the given code point is a valid Unicode code point.
	 * 
	 * A code point is valid if it is either between {@code 0x0000} and
	 * {@code 0xD7FF} or between {@code 0xE000} and {@code 0x10FFFF}.
	 * 
	 * @param codePoint
	 *            the code point to check
	 * @return whether or not the code point is valid
	 */
	public static boolean isValidCodePoint(int codePoint) {
		return (codePoint >= 0 && codePoint < 0xD800) || (codePoint >= 0xE000 && codePoint < 0x110000);
	}

	/**
	 * Gets the UTF-16 {@code String} representation of this
	 * {@code UnicodeString}.
	 * 
	 * @return the UTF-16 {@code String}
	 */
	@Override
	public String toString() {
		return new String(encodeUtf16(data));
	}

	private static int[] decodeUtf16(char[] utf16) {
		int[] utf32 = new int[utf16.length];
		int utf32Length = 0;
		for (int i = 0; i < utf16.length; i++) {
			char first = utf16[i];
			if (Character.isHighSurrogate(first)) {
				if (i == utf16.length - 1)
					throw new UnicodeException("missing low surrogate");
				if (!Character.isLowSurrogate(utf16[i + 1]))
					throw new UnicodeException("missing low surrogate");
				int codePoint = Character.toCodePoint(first, utf16[++i]);
				if (!isValidCodePoint(codePoint)) {
					long unsigned = (codePoint + (1L << 31)) % (1L << 32);
					throw new UnicodeException(String.format("invalid character %#04x", unsigned));
				}
				utf32[utf32Length++] = codePoint;
			} else if (Character.isLowSurrogate(first)) {
				throw new UnicodeException("missing high surrogate");
			} else {
				utf32[utf32Length++] = first;
			}
		}
		return Arrays.copyOf(utf32, utf32Length);
	}

	private static char[] encodeUtf16(int[] utf32) {
		char[] utf16 = new char[utf32.length * 2];
		int utf16Length = 0;
		for (int codePoint : utf32) {
			if ((0 <= codePoint && codePoint < 0xD800) || (0xE000 <= codePoint && codePoint < 0x10000)) {
				utf16[utf16Length++] = (char) codePoint;
			} else if (codePoint >= 0x10000 && codePoint < 0x110000) {
				utf16[utf16Length++] = Character.highSurrogate(codePoint);
				utf16[utf16Length++] = Character.lowSurrogate(codePoint);
			} else {
				long unsigned = (codePoint + (1L << 31)) % (1L << 32);
				throw new UnicodeException(String.format("invalid character %#04x", unsigned));
			}
		}
		return Arrays.copyOf(utf16, utf16Length);
	}

	/**
	 * Returns a {@code UnicodeString} that is a substring of this
	 * {@code UnicodeString}. The substring begins at the specified index and
	 * extends to the end of this string.
	 * 
	 * @param start
	 *            the beginning index, inclusive
	 * @return the specified substring
	 */
	public UnicodeString substring(int start) {
		return substring(start, data.length);
	}

	/**
	 * Returns a {@code UnicodeString} that is a substring of this
	 * {@code UnicodeString}. The substring begins at the specified index and
	 * extends to the character at index {@code endIndex - 1}.
	 * 
	 * @param start
	 *            the beginning index, inclusive
	 * @param end
	 *            the ending index, exclusive
	 * @return the specified substring
	 */
	public UnicodeString substring(int start, int end) {
		if (start < 0)
			throw new StringIndexOutOfBoundsException(start);
		if (end < start || end > data.length)
			throw new StringIndexOutOfBoundsException(end);
		return new UnicodeString(Arrays.copyOfRange(data, start, end));
	}

	/**
	 * Gets the code point of the character at the specified index of this
	 * {@code UnicodeString}.
	 * 
	 * @param pos
	 *            the index of the character
	 * @return the code point at the specified index
	 */
	public int codePointAt(int pos) {
		return data[pos];
	}

	/**
	 * Concatenates two {@code UnicodeString}s and returns the result.
	 * 
	 * @param other
	 *            the {@code UnicodeString} to concatenate with this one
	 * @return the concatenated {@code UnicodeString}
	 */
	public UnicodeString concat(UnicodeString other) {
		int[] newData = Arrays.copyOf(data, data.length + other.data.length);
		System.arraycopy(other.data, 0, newData, data.length, other.data.length);
		return new UnicodeString(newData);
	}

	/**
	 * Returns the length of this {@code UnicodeString}. This is equivalent to
	 * the number of code points in the string.
	 * 
	 * @return the length of this {@code UnicodeString}
	 */
	public int length() {
		return data.length;
	}

	/**
	 * <p>
	 * Returns whether or not this {@code UnicodeString} represents the same
	 * sequence of characters as the given {@code String}.
	 * </p>
	 * 
	 * <p>
	 * A {@code UnicodeString} and a {@code String} represent the same sequence
	 * of characters if and only if the UTF-16 representation of the
	 * {@code UnicodeString} is equal to the {@code String} (as defined by
	 * {@code String.equals(Object)}).
	 * </p>
	 * 
	 * @param string
	 *            the {@code String} to compare this {@code UnicodeString} against.
	 * @return whether or not the given {@code String} represents the same sequence of
	 *         characters as this {@code UnicodeString.}
	 */
	public boolean contentEquals(String string) {
		return string.equals(new String(encodeUtf16(data)));
	}

	/**
	 * <p>
	 * Returns whether or not this {@code UnicodeString} represents the same
	 * sequence of characters as the given {@code UnicodeString}.
	 * </p>
	 *
	 * <p>
	 * Two {@code UnicodeString}s represent the same sequence of characters if and only
	 * if they contain the same code points in the same order.
	 * </p>
	 *
	 * @param string
	 *            the {@code UnicodeString} to compare this {@code UnicodeString} against.
	 * @return whether or not the given {@code UnicodeString} represents the same sequence of
	 *         characters as this {@code UnicodeString.}
	 */
	public boolean contentEquals(UnicodeString string) {
		return Arrays.equals(data, string.data);
	}

	/**
	 * <p>
	 * Returns whether or not the given {@code Object} is equal to this
	 * {@code UnicodeString}.
	 * </p>
	 * 
	 * <p>
	 * An object is equal to this {@code UnicodeString} if and only if
	 * <ul>
	 * <li>the objects are of the same class and</li>
	 * <li>the {@link UnicodeString#contentEquals(UnicodeString)} method of this
	 * {@code UnicodeString} returns {@code true} for the other object.
	 * </ul>
	 * </p>
	 * 
	 * @return whether or not the given {@code Object} is equal to this
	 *         {@code UnicodeString}.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() == obj.getClass())
			return contentEquals((UnicodeString) obj);
		return false;
	}

	/**
	 * Returns a hash code for this {@code UnicodeString}. The hash code for a
	 * {@code UnicodeString} object is computed as
	 * 
	 * <blockquote>
	 * 
	 * <pre>
	 * s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]
	 * </pre>
	 * 
	 * </blockquote>
	 * 
	 * using {@code int} arithmetic, where {@code s[i]} is the <i>i</i>th code
	 * point of the string, {@code n} is the length of the string, and {@code ^}
	 * indicates exponentiation. (The hash value of the empty string is zero.)
	 *
	 * @return a hash code value for this object
	 */
	@Override
	public int hashCode() {
		int h = hash;
		if (h == 0 && data.length > 0) {
			for (int codePoint : data) {
				h = 31 * h + codePoint;
			}
			hash = h;
		}
		return h;
	}

	public boolean equalsIgnoreCase(UnicodeString other) {
		if (this == other)
			return true;
		if (other == null)
			return false;
		if (data.length != other.data.length)
			return false;
		int thisOffset = 0;
		int otherOffset = 0;
		int length = data.length;
		while (length-- > 0) {
			int c1 = data[thisOffset++];
			int c2 = other.data[otherOffset++];
			if (c1 == c2)
				continue;
			int u1 = Character.toUpperCase(c1);
			int u2 = Character.toUpperCase(c2);
			if (u1 == u2)
				continue;
			if (Character.toLowerCase(u1) == Character.toLowerCase(u2))
				continue;
			return false;
		}
		return true;
	}

	public UnicodeString toLowerCase() {
		int[] codePoints = new int[data.length];
		for (int i = 0; i < data.length; i++)
			codePoints[i] = Character.toLowerCase(data[i]);
		return new UnicodeString(codePoints);
	}

	public UnicodeString toUpperCase() {
		int[] codePoints = new int[data.length];
		for (int i = 0; i < data.length; i++)
			codePoints[i] = Character.toUpperCase(data[i]);
		return new UnicodeString(codePoints);
	}

	public int[] toCodePointArray() {
		return Arrays.copyOf(data, data.length);
	}

	public IntStream codePoints() {
		return Arrays.stream(data);
	}

	public void getCodePoints(int[] dest, int srcBegin, int srcEnd, int dstBegin) {
		if (srcBegin < 0)
			throw new StringIndexOutOfBoundsException(srcBegin);
		if (srcEnd > data.length)
			throw new StringIndexOutOfBoundsException(srcEnd);
		if (srcBegin > srcEnd)
			throw new StringIndexOutOfBoundsException(srcEnd - srcBegin);
		System.arraycopy(data, srcBegin, dest, dstBegin, srcEnd - srcBegin);
	}

	@Override
	public int compareTo(UnicodeString o) {
		int length = Math.min(data.length, o.data.length);
		for (int i = 0; i < length; i++) {
			int diff = data[i] - o.data[i];
			if (diff != 0)
				return diff;
		}
		return data.length - o.data.length;
	}
}
